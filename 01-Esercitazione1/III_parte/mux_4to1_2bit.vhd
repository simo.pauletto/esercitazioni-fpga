LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_4to1_2bit IS
	PORT (
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
		);
	END mux_4to1_2bit; 

ARCHITECTURE Structural OF mux_4to1_2bit IS
	BEGIN

	LEDR(9 DOWNTO 2) <= "00000000";
	
	bit0: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),SW(5),SW(3),SW(1),LEDR(1));
	bit1: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(SW(9),SW(8),SW(6),SW(4),SW(2),SW(0),LEDR(0));

	END Structural;