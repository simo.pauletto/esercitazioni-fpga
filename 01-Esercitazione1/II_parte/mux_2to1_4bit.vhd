LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_2to1_4bit IS
  PORT (
    SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
    );
  END mux_2to1_4bit; 

ARCHITECTURE Structural OF mux_2to1_4bit IS
  BEGIN
  
  LEDR(8 DOWNTO 4) <= "00000";
  LEDR(9) <= SW(9);
  
  bit3: ENTITY work.mux_2to1_1bit(Behavior)
    PORT MAP (SW(3),SW(7),SW(9),LEDR(3));
  bit2: ENTITY work.mux_2to1_1bit(Behavior)
    PORT MAP (SW(2),SW(6),SW(9),LEDR(2));
  bit1: ENTITY work.mux_2to1_1bit(Behavior)
    PORT MAP (SW(1),SW(5),SW(9),LEDR(1));
  bit0: ENTITY work.mux_2to1_1bit(Behavior)
    PORT MAP (SW(0),SW(4),SW(9),LEDR(0));
  END Structural;