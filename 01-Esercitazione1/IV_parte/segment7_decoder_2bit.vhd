LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY segment7_decoder_2bit IS
	PORT (
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END segment7_decoder_2bit; 

ARCHITECTURE Behavior OF segment7_decoder_2bit IS
	BEGIN
	HEX0(0) <= (NOT(SW(0)) AND NOT(SW(1))) OR SW(1);
	HEX0(1) <= SW(0);
	HEX0(2) <= SW(0);
	HEX0(3) <= SW(1);
	HEX0(4) <= SW(1);
	HEX0(5) <= (NOT(SW(0)) AND NOT(SW(1))) OR SW(1);
	HEX0(6) <= SW(1);
	END Behavior;