LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY segment7_decoder_2bit IS
	PORT (
		c0,c1 : IN STD_LOGIC;
		hex_display : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END segment7_decoder_2bit; 

ARCHITECTURE Behavior OF segment7_decoder_2bit IS
	BEGIN
	hex_display(0) <= (NOT(c0) AND NOT(c1)) OR c1;
	hex_display(1) <= c0;
	hex_display(2) <= c0;
	hex_display(3) <= c1;
	hex_display(4) <= c1;
	hex_display(5) <= (NOT(c0) AND NOT(c1)) OR c1;
	hex_display(6) <= c1;
	END Behavior;