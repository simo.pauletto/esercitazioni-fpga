LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY string_6display IS
	PORT (
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END string_6display; 

ARCHITECTURE Structural OF string_6display IS
	SIGNAL M0,M1,M2,M3,M4,M5 : STD_LOGIC_VECTOR(1 DOWNTO 0);
	BEGIN
	
	LEDR <= "0000000000";
	
	mux0: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('0','0'),('1','1'),('1','1'),('1','1'),('1','0'),('0','1'),M0);
	mux1: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('0','1'),('0','0'),('1','1'),('1','1'),('1','1'),('1','0'),M1);
	mux2: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('1','0'),('0','1'),('0','0'),('1','1'),('1','1'),('1','1'),M2);
	mux3: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('1','1'),('1','0'),('0','1'),('0','0'),('1','1'),('1','1'),M3);
	mux4: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('1','1'),('1','1'),('1','0'),('0','1'),('0','0'),('1','1'),M4);
	mux5: ENTITY work.mux_6to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),SW(7),('1','1'),('1','1'),('1','1'),('1','0'),('0','1'),('0','0'),M5);
		
	
	display0: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M0(0),M0(1),HEX5);
	display1: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M1(0),M1(1),HEX4);
	display2: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M2(0),M2(1),HEX3);
	display3: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M3(0),M3(1),HEX2);
	display4: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M4(0),M4(1),HEX1);
	display5: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M5(0),M5(1),HEX0);	
	
	END Structural;