LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_4to1_2bit IS
	PORT (
		s0,s1 : IN STD_LOGIC;
		U,V,W,X : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END mux_4to1_2bit; 

ARCHITECTURE Structural OF mux_4to1_2bit IS
	BEGIN
	
	bit0: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(s0,s1,U(1),V(1),W(1),X(1),M(1));
	bit1: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(s0,s1,U(0),V(0),W(0),X(0),M(0));

	END Structural;