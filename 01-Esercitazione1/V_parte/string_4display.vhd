LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY string_4display IS
	PORT (
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END string_4display; 

ARCHITECTURE Structural OF string_4display IS
	SIGNAL M0,M1,M2,M3 : STD_LOGIC_VECTOR(1 DOWNTO 0);
	BEGIN
	
	LEDR <= "0000000000";
	
	mux0: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),(SW(7),SW(6)),(SW(5),SW(4)),(SW(3),SW(2)),(SW(1),SW(0)),M0);
	mux1: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),(SW(5),SW(4)),(SW(3),SW(2)),(SW(1),SW(0)),(SW(7),SW(6)),M1);
	mux2: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),(SW(3),SW(2)),(SW(1),SW(0)),(SW(7),SW(6)),(SW(5),SW(4)),M2);
	mux3: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP(SW(9),SW(8),(SW(1),SW(0)),(SW(7),SW(6)),(SW(5),SW(4)),(SW(3),SW(2)),M3);
	
	display0: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M0(0),M0(1),HEX0);
	display1: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M1(0),M1(1),HEX1);
	display2: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M2(0),M2(1),HEX2);
	display3: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(M3(0),M3(1),HEX3);	
	
	END Structural;