LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY full_adder_4bit IS
	PORT(
		carry_in: IN STD_LOGIC;
		A,B: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		SUM: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		carry_out: OUT STD_LOGIC
		);
	END full_adder_4bit; 

ARCHITECTURE Structural OF full_adder_4bit IS
	SIGNAL c1,c2,c3: STD_LOGIC;
	BEGIN
	
	bit0: ENTITY work.full_adder_1bit(Mixed)
		PORT MAP(carry_in,A(0),B(0),SUM(0),c1);
	bit1: ENTITY work.full_adder_1bit(Mixed)
		PORT MAP(c1,A(1),B(1),SUM(1),c2);
	bit2: ENTITY work.full_adder_1bit(Mixed)
		PORT MAP(c2,A(2),B(2),SUM(2),c3);
	bit3: ENTITY work.full_adder_1bit(Mixed)
		PORT MAP(c3,A(3),B(3),SUM(3),carry_out);

	END Structural;