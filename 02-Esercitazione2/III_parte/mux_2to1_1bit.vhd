LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_2to1_1bit IS
    PORT(
      x,y,s : IN STD_LOGIC;
      m: OUT STD_LOGIC
		);
  END mux_2to1_1bit; 

ARCHITECTURE Behavior OF mux_2to1_1bit IS
  BEGIN
  m <= (NOT (s) AND x) OR (s AND y);
  END Behavior;