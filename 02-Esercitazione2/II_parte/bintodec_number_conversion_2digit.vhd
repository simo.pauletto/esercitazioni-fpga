LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY bintodec_number_conversion_2digit IS
	PORT (
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END bintodec_number_conversion_2digit; 

ARCHITECTURE Structural OF bintodec_number_conversion_2digit IS
SIGNAL MS_digit, LS_digit, A : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN

	LEDR <= "0000000000";
	
	comparator: ENTITY work.comparator_ge9(Behavior)
		PORT MAP((SW(3),SW(2),SW(1),SW(0)),MS_digit);
		
	LS_digit_selector: ENTITY work.first_digit_selector(Behavior)
		PORT MAP((SW(3),SW(2),SW(1),SW(0)),A);
	mux: ENTITY work.mux_2to1_4bit(Structural)
		PORT MAP(MS_digit(0),(SW(3),SW(2),SW(1),SW(0)),A,LS_digit);
	
	display0: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(LS_digit,HEX0);
	display1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(MS_digit,HEX1);
 
  END Structural;