LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY first_digit_selector IS
	PORT (
		number_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		number_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END first_digit_selector; 

ARCHITECTURE Behavior OF first_digit_selector IS
	BEGIN
	
	number_out <= 
		"0000" when number_in = "0000" else
		"0001" when number_in = "0001" else
		"0010" when number_in = "0010" else
		"0011" when number_in = "0011" else
		"0100" when number_in = "0100" else
		"0101" when number_in = "0101" else
		"0110" when number_in = "0110" else
		"0111" when number_in = "0111" else
		"1000" when number_in = "1000" else
		"1001" when number_in = "1001" else
		"0000" when number_in = "1010" else
		"0001" when number_in = "1011" else
		"0010" when number_in = "1100" else
		"0011" when number_in = "1101" else
		"0100" when number_in = "1110"else
		"0101" when number_in = "1111";
				
	END Behavior;