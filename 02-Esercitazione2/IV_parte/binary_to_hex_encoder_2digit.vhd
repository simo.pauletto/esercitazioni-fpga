LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY binary_to_hex_encoder_2digit IS
	PORT (
		carry_in : IN STD_LOGIC;
		num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		hex_display0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		hex_display1 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END binary_to_hex_encoder_2digit; 

ARCHITECTURE Structural OF binary_to_hex_encoder_2digit IS
	SIGNAL MS_digit, LS_digit, A : STD_LOGIC_VECTOR(3 DOWNTO 0);
	BEGIN
	
	comparator: ENTITY work.comparator_ge9(Behavior)
		PORT MAP((carry_in,num(3),num(2),num(1),num(0)),MS_digit);
		
	LS_digit_selector: ENTITY work.first_digit_selector(Behavior)
		PORT MAP((carry_in,num(3),num(2),num(1),num(0)),A);
	mux: ENTITY work.mux_2to1_4bit(Structural)
		PORT MAP(MS_digit(0),(num(3),num(2),num(1),num(0)),A,LS_digit);
	
	display0: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(LS_digit,hex_display0);
	display1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(MS_digit,hex_display1);
 
  END Structural;