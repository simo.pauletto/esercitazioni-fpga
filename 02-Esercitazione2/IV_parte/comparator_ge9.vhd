LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY comparator_ge9 IS
	PORT (
		number_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		number_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END comparator_ge9; 

ARCHITECTURE Behavior OF comparator_ge9 IS
	BEGIN
	
	number_out <= 
		"0000" when (
			(number_in = "00000") OR
			(number_in = "00001") OR
			(number_in = "00010") OR
			(number_in = "00011") OR
			(number_in = "00100") OR
			(number_in = "00101") OR
			(number_in = "00110") OR
			(number_in = "00111") OR
			(number_in = "01000") OR
			(number_in = "01001")
			) else "0001";
				
	END Behavior;