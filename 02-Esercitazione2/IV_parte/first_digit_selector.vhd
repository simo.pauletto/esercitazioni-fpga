LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY first_digit_selector IS
	PORT (
		number_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		number_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END first_digit_selector; 

ARCHITECTURE Behavior OF first_digit_selector IS
	BEGIN

	number_out <= 
		"0000" when number_in = "00000" else
		"0001" when number_in = "00001" else
		"0010" when number_in = "00010" else
		"0011" when number_in = "00011" else
		"0100" when number_in = "00100" else
		"0101" when number_in = "00101" else
		"0110" when number_in = "00110" else
		"0111" when number_in = "00111" else
		"1000" when number_in = "01000" else
		"1001" when number_in = "01001" else
		"0000" when number_in = "01010" else
		"0001" when number_in = "01011" else
		"0010" when number_in = "01100" else
		"0011" when number_in = "01101" else
		"0100" when number_in = "01110" else
		"0101" when number_in = "01111" else
		"0110" when number_in = "10000" else
		"0111" when number_in = "10001" else
		"1000" when number_in = "10010" else
		"1001" when number_in = "10011" else
		"0000";
				
	END Behavior;