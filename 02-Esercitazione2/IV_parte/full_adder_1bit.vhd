LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY full_adder_1bit IS
	PORT(
		carry_in, a, b : IN STD_LOGIC;
		sum, carry_out: OUT STD_LOGIC
		);
	END full_adder_1bit; 

ARCHITECTURE Mixed OF full_adder_1bit IS
	BEGIN
	sum <= ((a XOR b) XOR carry_in);

	carry:  ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP(b,carry_in,(a XOR b),carry_out);

	END Mixed;