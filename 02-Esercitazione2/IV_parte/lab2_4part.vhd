LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab2_4part IS
	PORT(
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END lab2_4part; 

ARCHITECTURE Structural OF lab2_4part IS
	SIGNAL SUM: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL carry: STD_LOGIC;
	SIGNAL error_ge9_x,error_ge9_y: STD_LOGIC_VECTOR(3 DOWNTO 0);
	BEGIN
	
	adder: ENTITY work.full_adder_4bit(Structural)
		PORT MAP(SW(8),(SW(7 DOWNTO 4)),(SW(3 DOWNTO 0)),SUM,carry);
	
	displayer: ENTITY work.binary_to_hex_encoder_2digit(Structural)
		PORT MAP(carry,SUM,HEX0,HEX1);
		
	LEDR(8) <= carry;
	LEDR(3 DOWNTO 0) <= SUM;
	
	error_ge9_0: ENTITY work.comparator_ge9(Behavior)
		PORT MAP(('0',SW(7),SW(6),SW(5),SW(4)),error_ge9_x);
	error_ge9_1: ENTITY work.comparator_ge9(Behavior)
		PORT MAP(('0',SW(3),SW(2),SW(1),SW(0)),error_ge9_y);
	
	LEDR(9) <= error_ge9_x(0) OR error_ge9_y(0);
	
	END Structural;