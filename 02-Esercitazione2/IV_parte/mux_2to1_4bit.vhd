LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_2to1_4bit IS
	PORT (
		s : IN STD_LOGIC;
		X,Y : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END mux_2to1_4bit; 

ARCHITECTURE Structural OF mux_2to1_4bit IS
BEGIN

	bit3: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(3),Y(3),s,M(3));
	bit2: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(2),Y(2),s,M(2));
	bit1: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(1),Y(1),s,M(1));
	bit0: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(0),Y(0),s,M(0));
 
  END Structural;