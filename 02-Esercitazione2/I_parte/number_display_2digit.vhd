LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY number_display_2digit IS
	PORT(
		SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END number_display_2digit; 

ARCHITECTURE Structural OF number_display_2digit IS
	BEGIN
	
	LEDR <= "0000000000";
	
	display0: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP((SW(3),SW(2),SW(1),SW(0)),HEX0);
	display1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP((SW(7),SW(6),SW(5),SW(4)),HEX1);
	
	END Structural;