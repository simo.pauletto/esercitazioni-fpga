LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY binary_to_hex_encoder IS
	PORT (
		sel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		hex_display : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END binary_to_hex_encoder; 

ARCHITECTURE Behavior OF binary_to_hex_encoder IS
	BEGIN
	
	hex_display <= 
		"0000001" when sel = "0000" else
      "1001111" when sel = "0001" else
      "0010010" when sel = "0010" else
      "0000110" when sel = "0011" else
      "1001100" when sel = "0100" else
      "0100100" when sel = "0101" else
      "0100000" when sel = "0110" else
      "0001111" when sel = "0111" else
      "0000000" when sel = "1000" else
      "0000100" when sel = "1001" else
      "1111111";
		
	END Behavior;