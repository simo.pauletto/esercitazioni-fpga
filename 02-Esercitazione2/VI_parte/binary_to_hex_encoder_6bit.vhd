LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY binary_to_hex_encoder_6bit IS
	PORT (
		sel : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
		hex_display_1 : OUT STD_LOGIC_VECTOR(0 TO 6);
		hex_display_0 : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END binary_to_hex_encoder_6bit; 

ARCHITECTURE Mixed OF binary_to_hex_encoder_6bit IS
	SIGNAL bin_digit0, bin_digit1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL s0,s1,s2,s3,s4,s5 : STD_LOGIC_VECTOR(1 DOWNTO 0);
	BEGIN
	
	conversion: PROCESS(sel) IS
	VARIABLE dec, dec_digit0, dec_digit1 : INTEGER;
	BEGIN
	
		s0 <= ('0',sel(0));
		s1 <= ('0',sel(1));
		s2 <= ('0',sel(2));
		s3 <= ('0',sel(3));
		s4 <= ('0',sel(4));
		s5 <= ('0',sel(5));
		
		dec := (
			(to_integer(unsigned(s0))*1) + 
			(to_integer(unsigned(s1))*2) + 
			(to_integer(unsigned(s2))*4) + 
			(to_integer(unsigned(s3))*8) + 
			(to_integer(unsigned(s4))*16) + 
			(to_integer(unsigned(s5))*32)
			);
			
		dec_digit1 := (dec / 10);
		dec_digit0 := (dec MOD 10);
		
		bin_digit1 <= std_logic_vector(to_unsigned(dec_digit1, bin_digit1'length));
		bin_digit0 <= std_logic_vector(to_unsigned(dec_digit0, bin_digit0'length));
	
	END PROCESS conversion;
	
	display0: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(bin_digit0,hex_display_0);
	display1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(bin_digit1,hex_display_1);
		
	END Mixed;