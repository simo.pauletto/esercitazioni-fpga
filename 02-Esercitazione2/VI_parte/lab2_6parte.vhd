LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY lab2_6parte IS
	PORT(
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6)
		);
END lab2_6parte; 

ARCHITECTURE Mixed OF lab2_6parte IS
BEGIN
	
	conversion: ENTITY work.binary_to_hex_encoder_6bit(Mixed)
		PORT MAP(SW(5 DOWNTO 0),HEX1,HEX0);
	
END Mixed;