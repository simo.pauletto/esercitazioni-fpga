LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY lab2_5parte IS
	PORT(
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6)
		);
END lab2_5parte; 

ARCHITECTURE Mixed OF lab2_5parte IS
SIGNAL T0,S0: STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL Z0,C1: STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
	
	number1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(SW(7 DOWNTO 4),HEX5);
	number2: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(SW(3 DOWNTO 0),HEX3);
		
	adder: PROCESS(SW) IS
	BEGIN
		T0 <= ('0' & SW(7 DOWNTO 4)) + ('0' & SW(3 DOWNTO 0)) + ('0','0','0','0',SW(8));
		IF (T0 > "1001") THEN
			Z0 <= "1010";
			C1 <= "0001";
		ELSE
			Z0 <= "0000";
			C1 <= "0000";
		END IF;
	END PROCESS adder;

	sum_MS_digit: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(C1,HEX1);
		
	S0 <= T0-Z0;
	sum_LS_digit: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(S0(3 DOWNTO 0),HEX0);
	
END Mixed;