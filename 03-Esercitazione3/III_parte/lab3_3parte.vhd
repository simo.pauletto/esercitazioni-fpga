LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab3_3parte IS
	PORT ( 
		Sw: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
		);
END lab3_3parte;

ARCHITECTURE Structural OF lab3_3parte IS
BEGIN
	
	latch: ENTITY work.flipflop_D_MAsterSlave(Structural)
		PORT MAP(SW(1),SW(0),LEDR(0));
	
END Structural;