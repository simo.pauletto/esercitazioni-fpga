LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY D_latch IS
	PORT ( 
		Clk, D : IN STD_LOGIC;
		Q_a, Q_b : OUT STD_LOGIC
		);
END D_latch;

ARCHITECTURE Behavior OF D_latch IS
	SIGNAL R_g, S_g,Qa, Qb, R, S : STD_LOGIC ;
	ATTRIBUTE KEEP : BOOLEAN;
	ATTRIBUTE KEEP OF R_g, S_g, Qa,Qb : SIGNAL IS TRUE;
BEGIN
	R <= NOT(D);
	S <= D;
	R_g <= NOT(R AND Clk);
	S_g <= NOT(S AND Clk);
	Qa <= NOT (R_g AND Qb);
	Qb <= NOT (S_g AND Qa);
	Q_a <= Qa;
	Q_b <= Qb;
END Behavior;