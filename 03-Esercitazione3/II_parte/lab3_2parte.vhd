LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab3_2parte IS
	PORT ( 
		Sw: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
		);
END lab3_2parte;

ARCHITECTURE Structural OF lab3_2parte IS
BEGIN
	
	latch: ENTITY work.D_latch(Behavior)
		PORT MAP(SW(1),SW(0),LEDR(0));
	
END Structural;