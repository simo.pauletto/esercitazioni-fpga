LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY flipflop_D_MAsterSlave IS
	PORT ( 
		Clk, D : IN STD_LOGIC;
		Q, Q_neg : OUT STD_LOGIC
		);
END flipflop_D_MAsterSlave;

ARCHITECTURE Structural OF flipflop_D_MAsterSlave IS
	SIGNAL Qm :STD_LOGIC;
BEGIN
	latch0: ENTITY work.D_latch(Behavior)
		PORT MAP(NOT(clk),D,Qm);
	latch1: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,Qm,Q,Q_neg);
END Structural;
