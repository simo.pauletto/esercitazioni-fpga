LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab3_4parte IS
	PORT ( 
		Clk, D : IN STD_LOGIC;
		Qa, Qa_neg, Qb, Qb_neg, Qc, Qc_neg : OUT STD_LOGIC
		);
END lab3_4parte;

ARCHITECTURE Structural OF lab3_4parte IS
	SIGNAL Qm :STD_LOGIC;
BEGIN
	latch: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D,Qa,Qa_neg);
	D_flipflop: ENTITY work.flipflop_D_MasterSlave(Structural)
		PORT MAP(clk,D,Qb,Qb_neg);
	D_flipflop_neg: ENTITY work.flipflop_D_MasterSlave(Structural)
		PORT MAP(NOT(clk),D,Qc,Qc_neg);
END Structural;