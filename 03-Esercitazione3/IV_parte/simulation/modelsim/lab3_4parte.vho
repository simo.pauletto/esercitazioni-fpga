-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.0 Build 711 06/05/2020 SJ Lite Edition"

-- DATE "11/14/2020 23:24:53"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	lab3_4parte IS
    PORT (
	Clk : IN std_logic;
	D : IN std_logic;
	Qa : BUFFER std_logic;
	Qa_neg : BUFFER std_logic;
	Qb : BUFFER std_logic;
	Qb_neg : BUFFER std_logic;
	Qc : BUFFER std_logic;
	Qc_neg : BUFFER std_logic
	);
END lab3_4parte;

-- Design Ports Information
-- Qa	=>  Location: PIN_AA26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qa_neg	=>  Location: PIN_AA30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qb	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qb_neg	=>  Location: PIN_AB30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qc	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qc_neg	=>  Location: PIN_AA28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Clk	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_W25,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF lab3_4parte IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_Clk : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_Qa : std_logic;
SIGNAL ww_Qa_neg : std_logic;
SIGNAL ww_Qb : std_logic;
SIGNAL ww_Qb_neg : std_logic;
SIGNAL ww_Qc : std_logic;
SIGNAL ww_Qc_neg : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \Clk~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \latch|R_g~combout\ : std_logic;
SIGNAL \latch|S_g~combout\ : std_logic;
SIGNAL \latch|Qb~combout\ : std_logic;
SIGNAL \latch|Qa~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|R_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|S_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|Qb~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|Qa~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|R_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|S_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|Qb~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|Qa~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|R_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|S_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|Qb~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|Qa~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|R_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|S_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|Qb~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|Qa~combout\ : std_logic;
SIGNAL \ALT_INV_D~input_o\ : std_logic;
SIGNAL \ALT_INV_Clk~input_o\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|ALT_INV_Qb~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|ALT_INV_Qb~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch0|ALT_INV_Qa~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch0|ALT_INV_Qa~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \latch|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|ALT_INV_Qa~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|ALT_INV_Qa~combout\ : std_logic;
SIGNAL \latch|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \latch|ALT_INV_Qa~combout\ : std_logic;
SIGNAL \D_flipflop_neg|latch1|ALT_INV_Qb~combout\ : std_logic;
SIGNAL \D_flipflop|latch1|ALT_INV_Qb~combout\ : std_logic;
SIGNAL \latch|ALT_INV_Qb~combout\ : std_logic;

BEGIN

ww_Clk <= Clk;
ww_D <= D;
Qa <= ww_Qa;
Qa_neg <= ww_Qa_neg;
Qb <= ww_Qb;
Qb_neg <= ww_Qb_neg;
Qc <= ww_Qc;
Qc_neg <= ww_Qc_neg;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_D~input_o\ <= NOT \D~input_o\;
\ALT_INV_Clk~input_o\ <= NOT \Clk~input_o\;
\D_flipflop_neg|latch0|ALT_INV_R_g~combout\ <= NOT \D_flipflop_neg|latch0|R_g~combout\;
\D_flipflop_neg|latch0|ALT_INV_S_g~combout\ <= NOT \D_flipflop_neg|latch0|S_g~combout\;
\D_flipflop|latch0|ALT_INV_S_g~combout\ <= NOT \D_flipflop|latch0|S_g~combout\;
\D_flipflop_neg|latch0|ALT_INV_Qb~combout\ <= NOT \D_flipflop_neg|latch0|Qb~combout\;
\D_flipflop|latch0|ALT_INV_Qb~combout\ <= NOT \D_flipflop|latch0|Qb~combout\;
\D_flipflop|latch0|ALT_INV_R_g~combout\ <= NOT \D_flipflop|latch0|R_g~combout\;
\D_flipflop_neg|latch0|ALT_INV_Qa~combout\ <= NOT \D_flipflop_neg|latch0|Qa~combout\;
\D_flipflop_neg|latch1|ALT_INV_R_g~combout\ <= NOT \D_flipflop_neg|latch1|R_g~combout\;
\D_flipflop|latch0|ALT_INV_Qa~combout\ <= NOT \D_flipflop|latch0|Qa~combout\;
\D_flipflop|latch1|ALT_INV_R_g~combout\ <= NOT \D_flipflop|latch1|R_g~combout\;
\latch|ALT_INV_R_g~combout\ <= NOT \latch|R_g~combout\;
\D_flipflop_neg|latch1|ALT_INV_S_g~combout\ <= NOT \D_flipflop_neg|latch1|S_g~combout\;
\D_flipflop_neg|latch1|ALT_INV_Qa~combout\ <= NOT \D_flipflop_neg|latch1|Qa~combout\;
\D_flipflop|latch1|ALT_INV_S_g~combout\ <= NOT \D_flipflop|latch1|S_g~combout\;
\D_flipflop|latch1|ALT_INV_Qa~combout\ <= NOT \D_flipflop|latch1|Qa~combout\;
\latch|ALT_INV_S_g~combout\ <= NOT \latch|S_g~combout\;
\latch|ALT_INV_Qa~combout\ <= NOT \latch|Qa~combout\;
\D_flipflop_neg|latch1|ALT_INV_Qb~combout\ <= NOT \D_flipflop_neg|latch1|Qb~combout\;
\D_flipflop|latch1|ALT_INV_Qb~combout\ <= NOT \D_flipflop|latch1|Qb~combout\;
\latch|ALT_INV_Qb~combout\ <= NOT \latch|Qb~combout\;

-- Location: IOOBUF_X89_Y23_N5
\Qa~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \latch|Qa~combout\,
	devoe => ww_devoe,
	o => ww_Qa);

-- Location: IOOBUF_X89_Y21_N22
\Qa_neg~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \latch|Qb~combout\,
	devoe => ww_devoe,
	o => ww_Qa_neg);

-- Location: IOOBUF_X89_Y23_N22
\Qb~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \D_flipflop|latch1|Qa~combout\,
	devoe => ww_devoe,
	o => ww_Qb);

-- Location: IOOBUF_X89_Y21_N5
\Qb_neg~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \D_flipflop|latch1|Qb~combout\,
	devoe => ww_devoe,
	o => ww_Qb_neg);

-- Location: IOOBUF_X89_Y21_N39
\Qc~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \D_flipflop_neg|latch1|Qa~combout\,
	devoe => ww_devoe,
	o => ww_Qc);

-- Location: IOOBUF_X89_Y21_N56
\Qc_neg~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \D_flipflop_neg|latch1|Qb~combout\,
	devoe => ww_devoe,
	o => ww_Qc_neg);

-- Location: IOIBUF_X89_Y20_N78
\Clk~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Clk,
	o => \Clk~input_o\);

-- Location: IOIBUF_X89_Y20_N44
\D~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: LABCELL_X88_Y21_N18
\latch|R_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \latch|R_g~combout\ = LCELL(( !\D~input_o\ & ( \Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \latch|R_g~combout\);

-- Location: LABCELL_X88_Y21_N21
\latch|S_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \latch|S_g~combout\ = LCELL(( \D~input_o\ & ( \Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \latch|S_g~combout\);

-- Location: LABCELL_X88_Y21_N30
\latch|Qb\ : cyclonev_lcell_comb
-- Equation(s):
-- \latch|Qb~combout\ = LCELL(( !\latch|Qa~combout\ & ( !\latch|S_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \latch|ALT_INV_S_g~combout\,
	dataf => \latch|ALT_INV_Qa~combout\,
	combout => \latch|Qb~combout\);

-- Location: LABCELL_X88_Y21_N33
\latch|Qa\ : cyclonev_lcell_comb
-- Equation(s):
-- \latch|Qa~combout\ = LCELL(( !\latch|Qb~combout\ & ( !\latch|R_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \latch|ALT_INV_R_g~combout\,
	dataf => \latch|ALT_INV_Qb~combout\,
	combout => \latch|Qa~combout\);

-- Location: LABCELL_X88_Y21_N48
\D_flipflop|latch0|R_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch0|R_g~combout\ = LCELL(( !\D~input_o\ & ( !\Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \D_flipflop|latch0|R_g~combout\);

-- Location: LABCELL_X88_Y21_N51
\D_flipflop|latch0|S_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch0|S_g~combout\ = LCELL(( \D~input_o\ & ( !\Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \D_flipflop|latch0|S_g~combout\);

-- Location: LABCELL_X88_Y21_N6
\D_flipflop|latch0|Qb\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch0|Qb~combout\ = LCELL(( !\D_flipflop|latch0|Qa~combout\ & ( !\D_flipflop|latch0|S_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \D_flipflop|latch0|ALT_INV_S_g~combout\,
	dataf => \D_flipflop|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop|latch0|Qb~combout\);

-- Location: LABCELL_X88_Y21_N9
\D_flipflop|latch0|Qa\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch0|Qa~combout\ = LCELL(( !\D_flipflop|latch0|Qb~combout\ & ( !\D_flipflop|latch0|R_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \D_flipflop|latch0|ALT_INV_R_g~combout\,
	dataf => \D_flipflop|latch0|ALT_INV_Qb~combout\,
	combout => \D_flipflop|latch0|Qa~combout\);

-- Location: LABCELL_X88_Y21_N57
\D_flipflop|latch1|R_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch1|R_g~combout\ = LCELL((\Clk~input_o\ & !\D_flipflop|latch0|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100010001000100010001000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	datab => \D_flipflop|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop|latch1|R_g~combout\);

-- Location: LABCELL_X88_Y21_N54
\D_flipflop|latch1|S_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch1|S_g~combout\ = LCELL((\Clk~input_o\ & \D_flipflop|latch0|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001000100010001000100010001000100010001000100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	datab => \D_flipflop|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop|latch1|S_g~combout\);

-- Location: LABCELL_X88_Y21_N36
\D_flipflop|latch1|Qb\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch1|Qb~combout\ = LCELL(( !\D_flipflop|latch1|Qa~combout\ & ( !\D_flipflop|latch1|S_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \D_flipflop|latch1|ALT_INV_S_g~combout\,
	dataf => \D_flipflop|latch1|ALT_INV_Qa~combout\,
	combout => \D_flipflop|latch1|Qb~combout\);

-- Location: LABCELL_X88_Y21_N39
\D_flipflop|latch1|Qa\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop|latch1|Qa~combout\ = LCELL((!\D_flipflop|latch1|R_g~combout\ & !\D_flipflop|latch1|Qb~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \D_flipflop|latch1|ALT_INV_R_g~combout\,
	datad => \D_flipflop|latch1|ALT_INV_Qb~combout\,
	combout => \D_flipflop|latch1|Qa~combout\);

-- Location: LABCELL_X88_Y21_N27
\D_flipflop_neg|latch0|R_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch0|R_g~combout\ = LCELL(( !\D~input_o\ & ( \Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \D_flipflop_neg|latch0|R_g~combout\);

-- Location: LABCELL_X88_Y21_N24
\D_flipflop_neg|latch0|S_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch0|S_g~combout\ = LCELL(( \D~input_o\ & ( \Clk~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Clk~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \D_flipflop_neg|latch0|S_g~combout\);

-- Location: LABCELL_X88_Y21_N12
\D_flipflop_neg|latch0|Qb\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch0|Qb~combout\ = LCELL(( !\D_flipflop_neg|latch0|Qa~combout\ & ( !\D_flipflop_neg|latch0|S_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \D_flipflop_neg|latch0|ALT_INV_S_g~combout\,
	dataf => \D_flipflop_neg|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop_neg|latch0|Qb~combout\);

-- Location: LABCELL_X88_Y21_N15
\D_flipflop_neg|latch0|Qa\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch0|Qa~combout\ = LCELL(( !\D_flipflop_neg|latch0|Qb~combout\ & ( !\D_flipflop_neg|latch0|R_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \D_flipflop_neg|latch0|ALT_INV_R_g~combout\,
	dataf => \D_flipflop_neg|latch0|ALT_INV_Qb~combout\,
	combout => \D_flipflop_neg|latch0|Qa~combout\);

-- Location: LABCELL_X88_Y21_N3
\D_flipflop_neg|latch1|R_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch1|R_g~combout\ = LCELL((!\Clk~input_o\ & !\D_flipflop_neg|latch0|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100010001000100010001000100010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	datab => \D_flipflop_neg|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop_neg|latch1|R_g~combout\);

-- Location: LABCELL_X88_Y21_N0
\D_flipflop_neg|latch1|S_g\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch1|S_g~combout\ = LCELL((!\Clk~input_o\ & \D_flipflop_neg|latch0|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010001000100010001000100010001000100010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Clk~input_o\,
	datab => \D_flipflop_neg|latch0|ALT_INV_Qa~combout\,
	combout => \D_flipflop_neg|latch1|S_g~combout\);

-- Location: LABCELL_X88_Y21_N42
\D_flipflop_neg|latch1|Qb\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch1|Qb~combout\ = LCELL((!\D_flipflop_neg|latch1|S_g~combout\ & !\D_flipflop_neg|latch1|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \D_flipflop_neg|latch1|ALT_INV_S_g~combout\,
	datad => \D_flipflop_neg|latch1|ALT_INV_Qa~combout\,
	combout => \D_flipflop_neg|latch1|Qb~combout\);

-- Location: LABCELL_X88_Y21_N45
\D_flipflop_neg|latch1|Qa\ : cyclonev_lcell_comb
-- Equation(s):
-- \D_flipflop_neg|latch1|Qa~combout\ = LCELL(( !\D_flipflop_neg|latch1|Qb~combout\ & ( !\D_flipflop_neg|latch1|R_g~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \D_flipflop_neg|latch1|ALT_INV_R_g~combout\,
	dataf => \D_flipflop_neg|latch1|ALT_INV_Qb~combout\,
	combout => \D_flipflop_neg|latch1|Qa~combout\);

-- Location: MLABCELL_X59_Y36_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


