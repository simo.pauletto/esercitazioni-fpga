LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY D_latch IS
	PORT ( 
		Clk, D, res : IN STD_LOGIC;
		Q_a, Q_b : OUT STD_LOGIC
		);
END D_latch;

ARCHITECTURE Behavior OF D_latch IS
	SIGNAL R_g, S_g,Qa, Qb, R, S : STD_LOGIC ;
	ATTRIBUTE KEEP : BOOLEAN;
	ATTRIBUTE KEEP OF R_g, S_g, Qa,Qb : SIGNAL IS TRUE;
BEGIN
	R <= NOT(D);
	S <= D;
	R_g <= NOT(R AND Clk);
	S_g <= NOT(S AND Clk);
	Qb <= NOT ((R_g AND res) AND Qa);
	Qa <= NOT ((S_g OR NOT(res)) AND Qb);
	Q_a <= Qa;
	Q_b <= Qb;
END Behavior;