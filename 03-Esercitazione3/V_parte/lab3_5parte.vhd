LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY lab3_5parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END lab3_5parte; 

ARCHITECTURE Mixed OF lab3_5parte IS
	SIGNAL A,B,SUM: STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
	
	reg: ENTITY work.reg8bit_latchD(Structural)
		PORT MAP(NOT(KEY(1)),KEY(0),SW(7 DOWNTO 0),A);
	
	B <= SW(7 DOWNTO 0);
	
	SUM <= A + B;
	
	numA: ENTITY work.enc7seg_8bit_to_hex(Structural)
		PORT MAP(A,HEX2,HEX3);
	numB: ENTITY work.enc7seg_8bit_to_hex(Structural)
		PORT MAP(B,HEX0,HEX1);
	numSum: ENTITY work.enc7seg_8bit_to_hex(Structural)
		PORT MAP(SUM,HEX4,HEX5);
		
END Mixed;