LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY reg8bit_latchD IS
	PORT ( 
		Clk, res: IN STD_LOGIC;
		D: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		Q, Q_neg: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
		);
END reg8bit_latchD;

ARCHITECTURE Structural OF reg8bit_latchD IS
BEGIN
	bit0: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(0),res,Q(0),Q_neg(0));
	bit1: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(1),res,Q(1),Q_neg(1));
	bit2: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(2),res,Q(2),Q_neg(2));
	bit3: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(3),res,Q(3),Q_neg(3));
	bit4: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(4),res,Q(4),Q_neg(4));
	bit5: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(5),res,Q(5),Q_neg(5));
	bit6: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(6),res,Q(6),Q_neg(6));
	bit7: ENTITY work.D_latch(Behavior)
		PORT MAP(clk,D(7),res,Q(7),Q_neg(7));
END Structural;