LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter4bit_to9 IS
	PORT ( 
		Clk, EN, Clr : IN STD_LOGIC;
		outs : OUT STD_LOGIC_VECTOR(3 DOWNTO 0):="0000"
		);
END counter4bit_to9;

ARCHITECTURE Behavior OF counter4bit_to9 IS
	SIGNAL Q : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN

	counter: PROCESS(Clk, EN, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= "0000";
			elsif EN = '1' then
				Q <= Q + 1;
				if Q = "1001" then 
					Q <= "0000";
				end if;
			end if;
		end if;  
			
	END PROCESS counter;
	
	outs <= Q;
	
END Behavior;