LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab4_3parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);	
		CLOCK_50: IN STD_LOGIC
		);
	END lab4_3parte;

ARCHITECTURE Structural OF lab4_3parte IS
	SIGNAL count32bit: STD_LOGIC_VECTOR(31 DOWNTO 0);
	SIGNAL count4bit: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL en: STD_LOGIC := '0';
BEGIN
	
	counterTo50M: ENTITY work.counter32bit_to50M(Behavior)
		PORT MAP(CLOCK_50, SW(0),SW(1),count32bit);
	
	process(count32bit) is
	begin
		if count32bit = "00000010111110101111000010000000" then
			en <= '1';
		else
			en <= '0';
		end if;
	end process;
	
	counterTo9: ENTITY work.counter4bit_to9(Behavior)
		PORT MAP(CLOCK_50,en,SW(1),count4bit);
		
	display: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP(count4bit,HEX0);
		
END Structural;