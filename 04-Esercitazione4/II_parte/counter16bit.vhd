LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter16bit IS
	PORT ( 
		Clk, EN, Clr : IN STD_LOGIC;
		Q : INOUT STD_LOGIC_VECTOR(15 DOWNTO 0):="0000000000000000"
		);
END counter16bit;

ARCHITECTURE Behavior OF counter16bit IS
BEGIN

	counter: PROCESS(Clk, EN, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= "0000000000000000";
			elsif EN = '1' then
				Q <= Q + 1;
			end if;
		end if;  
			
	END PROCESS counter;

END Behavior;