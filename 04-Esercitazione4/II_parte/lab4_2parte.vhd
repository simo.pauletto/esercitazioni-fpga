LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab4_2parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END lab4_2parte; 

ARCHITECTURE Structural OF lab4_2parte IS
	SIGNAL count: STD_LOGIC_VECTOR(15 DOWNTO 0);
BEGIN
	
	reg: ENTITY work.counter16bit(Behavior)
		PORT MAP(NOT(KEY(0)),SW(1),SW(0),count);
	
	numA: ENTITY work.enc7seg_16bit_to_hex(Structural)
		PORT MAP(count,HEX0,HEX1,HEX2,HEX3);
		
END Structural;