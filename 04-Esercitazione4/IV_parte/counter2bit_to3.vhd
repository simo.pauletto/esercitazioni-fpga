LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter2bit_to3 IS
	PORT ( 
		Clk, EN, Clr : IN STD_LOGIC;
		outs : OUT STD_LOGIC_VECTOR(1 DOWNTO 0):="00"
		);
END counter2bit_to3;

ARCHITECTURE Behavior OF counter2bit_to3 IS
 SIGNAL Q : STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN

	counter: PROCESS(Clk, EN, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= "00";
			elsif EN = '1' then
				Q <= Q + 1;
				if Q = "10" then
					Q <= "00";
				end if;
			end if;
		end if;  
			
	END PROCESS counter;

	outs <= Q;
	
END Behavior;