LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab4_4parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);	
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	END lab4_4parte;

ARCHITECTURE Structural OF lab4_4parte IS
	SIGNAL count32bit: STD_LOGIC_VECTOR(31 DOWNTO 0);
	SIGNAL count2bit,sel0,sel1,sel2: STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL en: STD_LOGIC := '0';
BEGIN
	
	counterTo50M: ENTITY work.counter32bit_to50M(Behavior)
		PORT MAP(CLOCK_50, SW(0),SW(1),count32bit);
	
	process(count32bit) is
	begin
		if count32bit = "00000010111110101111000010000000" then
			en <= '1';
		else
			en <= '0';
		end if;
	end process;
	
	counterTo3_0: ENTITY work.counter2bit_to3(Behavior)
		PORT MAP(CLOCK_50,en,SW(1),count2bit);
		
	mux0: entity work.mux_4to1_2bit(Structural)
		port map(count2bit,"10","00","01","11",sel0);
	mux1: entity work.mux_4to1_2bit(Structural)
		port map(count2bit,"01","10","00","11",sel1);
	mux2: entity work.mux_4to1_2bit(Structural)
		port map(count2bit,"00","01","10","11",sel2);
	
		
	display_0: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel0,HEX0);
	display_1: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel1,HEX1);
	display_2: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel2,HEX2);
		
END Structural;