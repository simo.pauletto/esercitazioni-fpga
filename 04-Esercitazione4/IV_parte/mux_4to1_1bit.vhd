LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_4to1_1bit IS
  PORT (
    s1,s0,u,v,w,x : IN STD_LOGIC;
    m : OUT STD_LOGIC
    );
  END mux_4to1_1bit; 

ARCHITECTURE Structural OF mux_4to1_1bit IS
	SIGNAL y0,y1 : STD_LOGIC;
	BEGIN
	mux1: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (u,w,s0,y0);
	mux2: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (v,x,s0,y1);
	mux3: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (y0,y1,s1,m);
	END Structural;