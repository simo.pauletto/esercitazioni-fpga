LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY counter8bit_ffT IS
	PORT ( 
		Clk, ENA, clear : IN STD_LOGIC;
		Q: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
		);
END counter8bit_ffT;

ARCHITECTURE Structural OF counter8bit_ffT IS
	SIGNAL outs, enables: STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN

	enables(0) <= ENA;
	bit0: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(0), clear, outs(0));
	
	enables(1) <= ENA AND outs(0);
	bit1: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(1), clear, outs(1));
	
	enables(2) <= enables(1) AND outs(1);
	bit2: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(2), clear, outs(2));
	
	enables(3) <= enables(2) AND outs(2);
	bit3: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(3), clear, outs(3));
	
	enables(4) <= enables(3) AND outs(3);	
	bit4: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(4), clear, outs(4));
	
	enables(5) <= enables(4) AND outs(4);
	bit5: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(5), clear, outs(5));
	
	enables(6) <= enables(5) AND outs(5);
	bit6: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(6), clear, outs(6));
	
	enables(7) <= enables(6) AND outs(7);
	bit7: ENTITY work.flipflop_T(Behavior)
		PORT MAP(Clk, enables(7), clear, outs(7));
		
	Q<= outs;
	
END Structural;
	