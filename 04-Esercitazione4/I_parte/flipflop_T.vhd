LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY flipflop_T IS
	PORT ( 
		Clk, T, clr : IN STD_LOGIC;
		Q: INOUT STD_LOGIC:='0';
		notQ: INOUT STD_LOGIC:='1'
		);
END flipflop_T;

ARCHITECTURE Behavior OF flipflop_T IS
BEGIN

	toggle: PROCESS(Clk, T, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= '0';
				notQ <= '1';
			elsif T = '1' then
				Q <= not(Q);
				notQ <= Q;
			end if;
		end if;  
			
	END PROCESS toggle;
	
END Behavior;