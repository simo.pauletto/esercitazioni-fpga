LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY lab4_1parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END lab4_1parte; 

ARCHITECTURE Structural OF lab4_1parte IS
	SIGNAL count: STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
	
	reg: ENTITY work.counter8bit_ffT(Structural)
		PORT MAP(NOT(KEY(0)),SW(1),SW(0),count);
	
	numA: ENTITY work.enc7seg_8bit_to_hex(Structural)
		PORT MAP(count,HEX0,HEX1);
		
END Structural;