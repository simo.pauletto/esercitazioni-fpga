LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter3bit_to6 IS
	PORT ( 
		Clk, EN, Clr : IN STD_LOGIC;
		outs : OUT STD_LOGIC_VECTOR(2 DOWNTO 0):="000"
		);
END counter3bit_to6;

ARCHITECTURE Behavior OF counter3bit_to6 IS
 SIGNAL Q : STD_LOGIC_VECTOR(2 DOWNTO 0);
BEGIN

	counter: PROCESS(Clk, EN, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= "000";
			elsif EN = '1' then
				Q <= Q + 1;
				if Q = "101" then
					Q <= "000";
				end if;
			end if;
		end if;  
			
	END PROCESS counter;

	outs <= Q;
	
END Behavior;