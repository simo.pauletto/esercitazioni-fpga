LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab4_5parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6);	
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	END lab4_5parte;

ARCHITECTURE Structural OF lab4_5parte IS
	SIGNAL count32bit: STD_LOGIC_VECTOR(31 DOWNTO 0);
	SIGNAL sel0,sel1,sel2,sel3,sel4,sel5: STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL count3bit: STD_LOGIC_VECTOR(2 DOWNTO 0);
	SIGNAL en: STD_LOGIC := '0';
BEGIN
	
	counterTo50M: ENTITY work.counter32bit_to50M(Behavior)
		PORT MAP(CLOCK_50, SW(0),SW(1),count32bit);
	
	process(count32bit) is
	begin
		if count32bit = "00000010111110101111000010000000" then
			en <= '1';
		else
			en <= '0';
		end if;
	end process;
	
	counterTo6: ENTITY work.counter3bit_to6(Behavior)
		PORT MAP(CLOCK_50,en,SW(1),count3bit);
		
	mux0: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"11","11","11","10","01","00",sel0);
	mux1: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"11","11","10","01","00","11",sel1);
	mux2: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"11","10","01","00","11","11",sel2);
	mux3: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"10","01","00","11","11","11",sel3);
	mux4: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"01","00","11","11","11","10",sel4);
	mux5: entity work.mux_6to1_2bit(Structural)
		port map(count3bit,"00","11","11","11","10","01",sel5);
	
	
		
	display_0: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel0,HEX0);
	display_1: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel1,HEX1);
	display_2: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel2,HEX2);
	display_3: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel3,HEX3);
	display_4: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel4,HEX4);
	display_5: ENTITY work.segment7_decoder_2bit(Behavior)
		PORT MAP(sel5,HEX5);
		
END Structural;