LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_2to1_2bit IS
	PORT (
		s : IN STD_LOGIC;
		X,Y : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END mux_2to1_2bit; 

ARCHITECTURE Structural OF mux_2to1_2bit IS
BEGIN

	bit1: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(1),Y(1),s,M(1));
	bit0: ENTITY work.mux_2to1_1bit(Behavior)
		PORT MAP (X(0),Y(0),s,M(0));
 
END Structural;