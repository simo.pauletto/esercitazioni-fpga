LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_4to1_2bit IS
	PORT (
		sel,A,B,C,D: in std_logic_vector(1 downto 0);
		outs : out std_logic_vector(1 downto 0)
		);
	END mux_4to1_2bit; 

ARCHITECTURE Structural OF mux_4to1_2bit IS
	BEGIN
	
	bit0: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(sel(1),sel(0),A(0),B(0),C(0),D(0),outs(0));
	bit1: ENTITY work.mux_4to1_1bit(Structural)
		PORT MAP(sel(1),sel(0),A(1),B(1),C(1),D(1),outs(1));

	END Structural;