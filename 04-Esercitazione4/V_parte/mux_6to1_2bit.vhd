LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_6to1_2bit IS
	PORT (
		s: in std_logic_vector(2 downto 0);
		U,V,W,X,Y,Z : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
		);
	END mux_6to1_2bit; 

ARCHITECTURE Structural OF mux_6to1_2bit IS
	SIGNAL M0,M1 : STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN
	
	mux0: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP((s(1),s(0)),U,V,W,X,M0);
	mux1: ENTITY work.mux_4to1_2bit(Structural)
		PORT MAP((s(1),s(0)),Y,Z,"11","11",M1);
	mux2: ENTITY work.mux_2to1_2bit(Structural)
		PORT MAP(s(2),M0,M1,M);

END Structural;