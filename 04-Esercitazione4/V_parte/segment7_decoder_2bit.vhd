LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY segment7_decoder_2bit IS
	PORT (
		c : IN STD_LOGIC_VECTOR(1 downto 0);
		hex_display : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
	END segment7_decoder_2bit; 

ARCHITECTURE Behavior OF segment7_decoder_2bit IS
	BEGIN
	hex_display(0) <= (NOT(c(0)) AND NOT(c(1))) OR c(1);
	hex_display(1) <= c(0);
	hex_display(2) <= c(0);
	hex_display(3) <= c(1);
	hex_display(4) <= c(1);
	hex_display(5) <= (NOT(c(0)) AND NOT(c(1))) OR c(1);
	hex_display(6) <= c(1);
	END Behavior;