LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter IS
	GENERIC ( n : NATURAL := 4; k : NATURAL := 20 );
	PORT ( 
		clock : IN STD_LOGIC;
		reset_n : IN STD_LOGIC;
		Q : OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0) := (OTHERS => '0');
		rollover : OUT STD_LOGIC := '0';
		set : in std_logic := '0';
		initial_value: in std_logic_vector(n-1 downto 0):= (OTHERS => '0')
		);
END ENTITY;

ARCHITECTURE Behavior OF counter IS
	SIGNAL count : STD_LOGIC_VECTOR(n-1 DOWNTO 0) := (OTHERS => '0');
BEGIN

	PROCESS (clock, reset_n, set)
	BEGIN
	
		IF (reset_n = '0') THEN
			count<= (OTHERS => '0');
		ELSIF (set = '0') THEN
			count <= initial_value;
		ELSIF ((clock'event) AND (clock = '1')) THEN
			count <= count + 1;
			if count = k-1 then
				count <= (OTHERS => '0');
				rollover <= '1';
			else
				rollover <= '0';
			end if;
		END IF;
	END PROCESS;
	
	Q <= count;
	
END Behavior;