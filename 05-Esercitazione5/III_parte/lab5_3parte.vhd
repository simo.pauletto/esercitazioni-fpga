LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY lab5_3parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);

		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	END lab5_3parte;

ARCHITECTURE Structural OF lab5_3parte IS
	signal count_dec,count_cent,count_sec,count_decsec: std_logic_vector(3 downto 0);
	signal count_min: std_logic_vector(3 downto 0):= "0000";
	signal count_decmin : std_logic_vector(3 downto 0):= "0000";
	signal initial_min: std_logic_vector(3 downto 0) := "0000";
	signal initial_decmin: std_logic_vector(3 downto 0) := "0000";
	signal clk,dec,cent,sec,decsec,min,decmin,xxx: std_logic;
	signal int: integer := 0;
	signal dec_digit1: integer:=0;
	signal dec_digit0: integer:=0;
	signal my_nrest : std_logic;
BEGIN
	
	int <= to_integer(signed(SW(7 downto 0)));
		
	dec_digit1 <= (int / 10);
	dec_digit0 <= (int MOD 10);
	
	initial_decmin <= std_logic_vector(to_unsigned(dec_digit1, initial_decmin'length));
	initial_min <= std_logic_vector(to_unsigned(dec_digit0, initial_min'length));

	reset:  -- added this procedure to remove FF from HEX4,5
	process (CLOCK_50) is
	  variable count : std_logic_vector(3 downto 0) := x"0"; 
	begin  
	  if rising_edge(CLOCK_50) then
		  if count<x"f" then
				my_nrest<='0';
				count:=count+1;
		  elsif count=x"f" then
				my_nrest<=KEY(0);
		  end if;
	  end if;
	end process;
	
	clock_dec: entity work.counter32bit_to50M(behavior)
		generic map(500000)
		port map(clk=>CLOCK_50,en=>KEY(1),clr=>my_nrest,rollover=>clk);
	
	counter_dec: entity work.counter(Behavior)
		generic map (4,10)
		port map(clk,my_nrest,count_cent,dec,KEY(2),"0000");
		
	counter_cent: entity work.counter(Behavior)
		generic map (4,10)
		port map(dec,my_nrest,count_dec,sec,KEY(2),"0000");
		
	counter_sec: entity work.counter(Behavior)
		generic map (4,10)
		port map(sec,my_nrest,count_sec,decsec,KEY(2),"0000");
		
	counter_decsec: entity work.counter(Behavior)
		generic map (4,6)
		port map(decsec,my_nrest,count_decsec,min,KEY(2),"0000");
		
	counter_min: entity work.counter(Behavior)
		generic map (4,10)
		port map(min,my_nrest,count_min,decmin,KEY(2),initial_min);
		
	counter_decmin: entity work.counter(Behavior)
		generic map (4,6)
		port map(decmin,my_nrest,count_decmin,xxx,KEY(2),initial_decmin);
	
	
	digit_dec: entity work.binary_to_hex_encoder(Behavior)
		port map(count_cent,HEX0);
	digit_cent: entity work.binary_to_hex_encoder(Behavior)
		port map(count_dec,HEX1);
	digit_sec: entity work.binary_to_hex_encoder(Behavior)
		port map(count_sec,HEX2);
	digit_decsec: entity work.binary_to_hex_encoder(Behavior)
		port map(count_decsec,HEX3);
	digit_min: entity work.binary_to_hex_encoder(Behavior)
		port map(count_min,HEX4);
	digit_decmin: entity work.binary_to_hex_encoder(Behavior)
		port map(count_decmin,HEX5);

	
END Structural;