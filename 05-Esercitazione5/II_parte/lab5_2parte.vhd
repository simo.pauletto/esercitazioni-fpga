LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab5_2parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);

		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	END lab5_2parte;

ARCHITECTURE Structural OF lab5_2parte IS
	signal count0,count1,count2: std_logic_vector(3 downto 0);
	signal roll0,roll1, roll2: std_logic;
BEGIN
	
	clock_1s: entity work.counter32bit_to50M(behavior)
		port map(clk =>CLOCK_50,en=>'1',clr=>KEY(0),rollover=>roll0);
	
	counter_0: entity work.counter(Behavior)
		generic map (4,10)
		port map(roll0,KEY(0),count0,roll1);
		
	counter_1: entity work.counter(Behavior)
		generic map (4,10)
		port map(roll1,KEY(0),count1,roll2);
		
	counter_2: entity work.counter(Behavior)
		generic map (4,10)
		port map(roll2,KEY(0),count2,LEDR(9));
	
	digit0: entity work.binary_to_hex_encoder(Behavior)
		port map(count0,HEX0);
	digit1: entity work.binary_to_hex_encoder(Behavior)
		port map(count1,HEX1);
	digit2: entity work.binary_to_hex_encoder(Behavior)
		port map(count2,HEX2);

	
END Structural;