LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter IS
	GENERIC ( n : NATURAL := 4; k : NATURAL := 20 );
	PORT ( 
		clock : IN STD_LOGIC;
		reset_n : IN STD_LOGIC;
		Q : OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0);
		rollover : OUT STD_LOGIC := '0'
		);
END ENTITY;

ARCHITECTURE Behavior OF counter IS
	SIGNAL value : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
BEGIN

	PROCESS (clock, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
			value <= (OTHERS => '0');
		ELSIF ((clock'EVENT) AND (clock = '1' )) THEN
			value <= value + 1;
			if value = k-1 then
				value <= (OTHERS => '0');
				rollover <= '1';
			else
				rollover <= '0';
			end if;
		END IF;
	END PROCESS;
	
	Q <= value;
	
END Behavior;