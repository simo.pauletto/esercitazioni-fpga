LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab5_1parte IS
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	END lab5_1parte;

ARCHITECTURE Structural OF lab5_1parte IS
	signal count: std_logic_vector(7 downto 0);
BEGIN
	
	counterTo19: entity work.counter(Behavior)
		generic map (8,20)
		port map(KEY(0),KEY(1),count,LEDR(9));
		
	LEDR(7 downto 0) <= count;
	
END Structural;