LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY reg8bit IS
	PORT ( 
		Clk, res: IN STD_LOGIC;
		D: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		Q: OUT STD_LOGIC_VECTOR(7 DOWNTO 0):="00000000"
		);
END reg8bit;

ARCHITECTURE behavior OF reg8bit IS
BEGIN
	process(clk, res) is
	begin
		if (res = '0') then
			Q <= "00000000";
		else
			if rising_edge(clk) then
				Q <= D;
			end if;
		end if;
	end process;
	
END behavior;