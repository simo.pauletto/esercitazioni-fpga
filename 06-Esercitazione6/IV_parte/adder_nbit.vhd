library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;


entity adder_nbit is
	generic ( n : natural);
	port (
		carry_in: in std_logic;
		A,B : in std_logic_vector(n-1 downto 0);
		S: out std_logic_vector(n-1 downto 0);
		carry_out: out std_logic
	);
end entity;

architecture behavior of adder_nbit is
	signal sum,num1,num2: std_logic_vector(n downto 0);
begin
	num1 <= ('0' & A);
	num2 <= ('0' & B);
	sum <= (num1 + num2) + carry_in;
	
	S <= sum(n-1 downto 0);
	carry_out <= sum(n);
	
end behavior;