LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY enc7seg_16bit_to_hex IS
	PORT ( 
		D: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		hex0,hex1,hex2,hex3: OUT STD_LOGIC_VECTOR(0 TO 6)
		);
END enc7seg_16bit_to_hex;

ARCHITECTURE Structural OF enc7seg_16bit_to_hex IS
BEGIN
	display3: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP (D(15 DOWNTO 12),hex3);
	display2: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP (D(11 DOWNTO 8),hex2);
	display1: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP (D(7 DOWNTO 4),hex1);
	display0: ENTITY work.binary_to_hex_encoder(Behavior)
		PORT MAP (D(3 DOWNTO 0),hex0);	
END Structural;