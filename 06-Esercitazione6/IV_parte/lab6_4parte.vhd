LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab6_4parte IS

	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);

		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab6_4parte;


ARCHITECTURE structural OF lab6_4parte IS
	signal A,B: std_logic_vector(7 downto 0);
	signal P, P_stored: std_logic_vector(15 downto 0);
BEGIN

	registerA: entity work.reg_nbit(behavior)
		generic map(8)
		port map(KEY(1),KEY(0),SW(8),SW(7 downto 0),A);
		
	registerB: entity work.reg_nbit(behavior)
		generic map(8)
		port map(KEY(1),KEY(0),SW(9),SW(7 downto 0),B);
		
	LEDR(7 downto 0) <= 
		A when SW(8)='1' else
		B when SW(9)='1' else 
		"00000000";
		
	multiplier: entity work.multiplier_8bit(structural)
		port map(A,B,P);
	
	register_out: entity work.reg_nbit(behavior)
		generic map(16)
		port map(KEY(1),KEY(0),'1',P,P_stored);
		
	display: entity work.enc7seg_16bit_to_hex(structural)
		port map(P_stored,HEX0,HEX1,HEX2,HEX3);

END structural;