library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity multiplier_8bit is
	port (
		A,B : in std_logic_vector(7 downto 0);
		M: out std_logic_vector(15 downto 0)
	);
end entity;

architecture structural of multiplier_8bit is
	type mat_8 is array (7 downto 0) of std_logic_vector(7 downto 0);
	signal and_b:mat_8;
	signal s0,s1,s2,s3,s4,s5,s6,s7: std_logic_vector(7 downto 0);
	signal c: std_logic_vector(6 downto 0);

begin

	process(A,B) is
		variable b_i: std_logic_vector(7 downto 0);
	begin
		for i in 0 to 7 loop
			b_i := (others => B(i));
			and_b(i) <=  A and b_i;
		end loop;
	end process;
	
	adder1: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',('0',and_b(0)(7),and_b(0)(6),and_b(0)(5),and_b(0)(4),and_b(0)(3),and_b(0)(2),and_b(0)(1)),and_b(1),s0,c(0));
		
	adder2: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(0) & s0(7 downto 1)),and_b(2),s1,c(1));
	
	adder3: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(1) & s1(7 downto 1)),and_b(3),s2,c(2));
		
	adder4: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(2) & s2(7 downto 1)),and_b(4),s3,c(3));
	
	adder5: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(3) & s3(7 downto 1)),and_b(5),s4,c(4));
		
	adder6: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(4) & s4(7 downto 1)),and_b(6),s5,c(5));
		
	adder7: entity work.adder_nbit(behavior)
		generic map(8)
		port map('0',(c(5) & s5(7 downto 1)),and_b(7),s6,c(6));
		
	M <= (c(6) & s6 & s5(0) & s4(0) & s3(0) & s2(0) & s1(0) & s0(0) & and_b(0)(0));
	
end structural;