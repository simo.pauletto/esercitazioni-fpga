LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY accumulator_8bit IS
	PORT (
		clk, res : in std_logic;
		A : in std_logic_vector(7 downto 0);
		SUM: out std_logic_vector(7 downto 0);
		carry, overflow: out std_logic
		);
	END accumulator_8bit;

ARCHITECTURE Structural OF accumulator_8bit IS
	signal A_reg, S, S_reg: std_logic_vector(7 downto 0);
	signal c_adder,overflow_logic: std_logic;
BEGIN

	reg_1: entity work.reg8bit(behavior)
		port map(NOT(clk),res,A,A_reg);
	
	adder: entity work.adder_nbit(behavior)
		generic map (8)
		port map('0',A_reg,S_reg,S,c_adder);
		
	reg_2: entity work.reg8bit(behavior)
		port map(NOT(clk),res,S,S_reg);
	
	SUM<=S_reg;
		
	overflow_logic <= '1' 
		when ((((A_reg > 0) AND (S_reg > 0)) AND (S < 0)) OR (((A_reg < 0) AND (S_reg < 0)) AND (S > 0))) 
		else '0';
	
	carry_latch: entity work.D_latch(behavior)
		port map(NOT(clk),c_adder,res,carry);
		
	overflow_latch: entity work.D_latch(behavior)
		port map(NOT(clk),overflow_logic,res,overflow);
	
	
END Structural;