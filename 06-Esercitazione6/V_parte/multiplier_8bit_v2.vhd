library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity multiplier_8bit_v2 is
	port (
		A,B : in std_logic_vector(7 downto 0);
		M: out std_logic_vector(15 downto 0)
	);
end entity;

architecture structural of multiplier_8bit_v2 is
	type mat_8 is array (7 downto 0) of std_logic_vector(15 downto 0);
	signal add:mat_8;
	signal p1_1,p1_2,p1_3,p1_4,p2_1,p2_2: std_logic_vector(15 downto 0);
	--signal A_temp, d: std_logic_vector(15 downto 0);

begin

	process(A,B) is
		variable A_temp_u: unsigned(15 downto 0);
		variable A_temp: std_logic_vector(15 downto 0);
	begin
		
		for i in 0 to 7 loop
			A_temp := ("00000000" & A);
			A_temp_u := unsigned(A_temp) sll i;
			if B(i) = '1' then 
				add(i) <= std_logic_vector(A_temp_u);
			else
				add(i) <= "0000000000000000";
			end if;
		end loop;
	end process;
	
	adder1_1: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',add(0),add(1),p1_1);
	
	adder1_2: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',add(2),add(3),p1_2);
		
	adder1_3: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',add(4),add(5),p1_3);
		
	adder1_4: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',add(6),add(7),p1_4);
		
		
		
	adder2_1: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',p1_1,p1_2,p2_1);
		
	adder2_2: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',p1_3,p1_4,p2_2);
		
		
	final_adder: entity work.adder_nbit(behavior)
		generic map(16)
		port map('0',p2_1,p2_2,M);
		
end structural;