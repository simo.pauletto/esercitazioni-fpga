LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab7_3parte IS

	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab7_3parte;


ARCHITECTURE structural OF lab7_3parte IS
	signal reg0_out, reg1_out: std_logic_vector(3 downto 0);
BEGIN
	
	reg_0: entity work.shift_register_nbit(behavior)
		generic map(4)
		port map(not(SW(1)),KEY(0),reg0_out);
		
	reg_1: entity work.shift_register_nbit(behavior)
		generic map(4)
		port map(SW(1),KEY(0),reg1_out);
		
	LEDR(3 downto 0) <= reg0_out;
	LEDR(7 downto 4) <= reg1_out;
	
	LEDR(9) <= reg0_out(3) or reg1_out(3);
	
END structural;