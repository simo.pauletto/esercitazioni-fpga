library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity shift_register_nbit is
	generic ( n : natural);
	port (
		en, clk: in std_logic;
		outs: inout std_logic_vector(n-1 downto 0) := (others =>'0')
	);
end entity;

architecture behavior of shift_register_nbit is
begin
	
	process (clk) is
	begin
		if rising_edge(clk) then
			if en = '1' then
				if outs = "0000" then 
					outs <= "0001";
				elsif outs = "1000" then 
					outs <= "1000";
				else
					outs <= std_logic_vector((unsigned(outs) sll 1));
				end if;
			elsif en = '0' then
				outs <= (others =>'0');
			end if;
		end if;
	end process;
	
end behavior;