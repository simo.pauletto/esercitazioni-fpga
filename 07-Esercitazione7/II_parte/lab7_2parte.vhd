LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab7_2parte IS

	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab7_2parte;


ARCHITECTURE structural OF lab7_2parte IS
	TYPE State_type IS (A, B, C, D, E, F, G, H, I);
	--Attribute to declare a specific encoding for the states
		attribute syn_encoding : string;
		attribute syn_encoding of State_type : type is "0000 0001 0010 0011 0100 0101 0110 0111 1000";
	signal y_Q,y_D : State_type;
	signal w: std_logic;
BEGIN
	w <= SW(1);

	PROCESS (w, y_Q) is --state table
	BEGIN
		case y_Q IS
			WHEN A => IF (w = '0') THEN y_D <= B;
				ELSE y_D <= F;
				END IF;
			WHEN B => IF (w = '0') THEN y_D <= C;
				ELSE y_D <= F;
				END IF;
			WHEN C => IF (w = '0') THEN y_D <= D;
				ELSE y_D <= F;
				END IF;
			WHEN D => IF (w = '0') THEN y_D <= E;
				ELSE y_D <= F;
				END IF;
			WHEN E => IF (w = '0') THEN y_D <= E;
				ELSE y_D <= F;
				END IF;
			WHEN F => IF (w = '0') THEN y_D <= B;
				ELSE y_D <= G;
				END IF;
			WHEN G => IF (w = '0') THEN y_D <= B;
				ELSE y_D <= H;
				END IF;
			WHEN H => IF (w = '0') THEN y_D <= B;
				ELSE y_D <= I;
				END IF;
			WHEN I => IF (w = '0') THEN y_D <= B;
				ELSE y_D <= I;
				END IF;
		END CASE;
	END PROCESS; --state table
	
	PROCESS (KEY(0)) --state flip-flops
	BEGIN
		if rising_edge(KEY(0)) then
			y_Q <= y_D;
		end if;
	END PROCESS;
	
	LEDR(9) <= '1' when (y_Q = E or y_Q = I) else '0';
	LEDR(8) <= '1' when (y_Q = I) else '0';
	LEDR(7) <= '1' when (y_Q = H) else '0';
	LEDR(6) <= '1' when (y_Q = G) else '0';
	LEDR(5) <= '1' when (y_Q = F) else '0';
	LEDR(4) <= '1' when (y_Q = E) else '0';
	LEDR(3) <= '1' when (y_Q = D) else '0';
	LEDR(2) <= '1' when (y_Q = C) else '0';
	LEDR(1) <= '1' when (y_Q = B) else '0';
	LEDR(0) <= '1' when (y_Q = A) else '0';
	
END structural;