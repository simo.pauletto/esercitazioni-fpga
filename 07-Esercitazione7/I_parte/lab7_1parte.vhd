LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab7_1parte IS

	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab7_1parte;


ARCHITECTURE structural OF lab7_1parte IS
	signal yA,yB,yC,yD,yE,yF,yG,yH,yI : std_logic;
BEGIN

	ff_A: entity work.D_latch(behavior)
		port map(KEY(0),SW(0),'0',yA);
		
		
	
	ff_B: entity work.D_latch(behavior)
		port map(KEY(0),(yF or yG or yH or yI or yA) and not(SW(1)),'0',yB);
	
	f_C: entity work.D_latch(behavior)
		port map(KEY(0),yB and not(SW(1)),'0',yC);
		
	f_D: entity work.D_latch(behavior)
		port map(KEY(0),yC and not(SW(1)),'0',yD);
	
	f_E: entity work.D_latch(behavior)
		port map(KEY(0),(yD or yE) and not(SW(1)),'0',yE);
		
	
	f_F: entity work.D_latch(behavior)
		port map(KEY(0),(yA or yB or yC or yD or yE) and SW(1),'0',yF);
	
	f_G: entity work.D_latch(behavior)
		port map(KEY(0),yF and SW(1),'0',yG);
	
	f_H: entity work.D_latch(behavior)
		port map(KEY(0),yG and SW(1),'0',yH);
		
	f_I: entity work.D_latch(behavior)
		port map(KEY(0),(yH or yI) and SW(1),'0',yI);
		
	LEDR(9) <= (yE or yI);
	LEDR(8 downto 0) <= (yI,yH,yG,yF,yE,yD,yC,yB,yA);
	
END structural;