LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab8_3parte IS
	
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab8_3parte;


ARCHITECTURE structural OF lab8_3parte IS
	signal q: STD_LOGIC_VECTOR (3 DOWNTO 0);
BEGIN
	
	ram: entity work.ram32x4_v2(behavior)
		port map(SW(8 downto 4),KEY(0),SW(3 downto 0),SW(9),q);
		
	input: entity work.binary_to_hex_encoder(behavior)
		port map(SW(3 downto 0),HEX2);
		
	output: entity work.binary_to_hex_encoder(behavior)
		port map(q,HEX0);
		
	add_MSBits: entity work.binary_to_hex_encoder(behavior)
		port map(('0','0','0',SW(8)),HEX5);
	
	add_LSBits: entity work.binary_to_hex_encoder(behavior)
		port map(SW(7 downto 4),HEX4);
	
	
END structural;