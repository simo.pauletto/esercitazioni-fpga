LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY ram32x4_v2 IS
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
	);
END ram32x4_v2;

ARCHITECTURE behavior OF ram32x4_v2 IS
	TYPE mem IS ARRAY(0 TO 31) OF STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL memory_array : mem;
BEGIN

	process (clock) is
	begin
		if rising_edge(clock) then
			if (wren = '1') then
				memory_array(to_integer(unsigned(address))) <= data;
			else
				q <= memory_array(to_integer(unsigned(address)));
			end if;
		end if;
	end process;

END behavior;