LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY counter32bit_to50M IS
	GENERIC (n: NATURAL := 50000000);
	PORT ( 
		Clk, EN, Clr : IN STD_LOGIC;
		outs : OUT STD_LOGIC_VECTOR(31 DOWNTO 0):="00000000000000000000000000000000";
		rollover: out std_logic
		);
END counter32bit_to50M;

ARCHITECTURE Behavior OF counter32bit_to50M IS
 SIGNAL Q : STD_LOGIC_VECTOR(31 DOWNTO 0);
BEGIN

	counter: PROCESS(Clk, EN, clr) IS
	BEGIN
		
		if rising_edge(Clk) then
			if Clr = '0' then
				Q <= "00000000000000000000000000000000";
			elsif EN = '1' then
				Q <= Q + 1;
				if Q = n-1 then
					Q <= "00000000000000000000000000000000";
					rollover <= '1';
				else
					rollover <= '0';
				end if;
			end if;
		end if;  
			
	END PROCESS counter;

	outs <= Q;
	
END Behavior;