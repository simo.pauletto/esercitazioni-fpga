LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab8_4parte IS
	
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX3: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX4: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX5: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab8_4parte;


ARCHITECTURE structural OF lab8_4parte IS
	signal raddress,waddress: STD_LOGIC_VECTOR (4 DOWNTO 0);
	signal q: STD_LOGIC_VECTOR (3 DOWNTO 0);
	signal clk_s: STD_LOGIC;
BEGIN
	
	waddress <= SW(8 downto 4);
	
	clock_sec: entity work.counter32bit_to50M(behavior)
		generic map(50000000)
		port map(clk=>CLOCK_50,en=>'1',clr=>KEY(0),rollover=>clk_s);
	
	counter_to32: entity work.counter(Behavior)
		generic map (5,31)
		port map(clk_s,KEY(0),raddress);
	
	
	
	ram: entity work.ram32x4_2port(syn)
		port map(CLOCK_50,SW(3 downto 0),raddress,waddress,SW(9),q);
		
	
	
	write_data: entity work.binary_to_hex_encoder(behavior)
		port map(SW(3 downto 0),HEX1);
		
	wadd_MSBits: entity work.binary_to_hex_encoder(behavior)
		port map(('0','0','0',waddress(4)),HEX5);
	
	wadd_LSBits: entity work.binary_to_hex_encoder(behavior)
		port map(waddress(3 downto 0),HEX4);
		
		
	
	read_data: entity work.binary_to_hex_encoder(behavior)
		port map(q,HEX0);
		
	radd_MSBits: entity work.binary_to_hex_encoder(behavior)
		port map(('0','0','0',raddress(4)),HEX3);
	
	radd_LSBits: entity work.binary_to_hex_encoder(behavior)
		port map(raddress(3 downto 0),HEX2);
			
		
	
END structural;