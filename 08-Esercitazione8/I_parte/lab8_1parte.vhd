LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab8_1parte IS
	
	PORT (
		address		: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
		);
	
END lab8_1parte;


ARCHITECTURE structural OF lab8_1parte IS
BEGIN
	
	ram: entity work.ram32x4(SYN)
		port map(address,clock,data,wren,q);
	
END structural;