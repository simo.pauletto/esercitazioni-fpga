LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

ENTITY dicotomic_search_FSM IS
	PORT (
		clk, en, start : in std_logic;
		n_in : in std_logic_vector(7 downto 0);
		address : out std_logic_vector(7 downto 0):="00000000";
		found : out std_logic
		);
	
END dicotomic_search_FSM;


ARCHITECTURE behavior OF dicotomic_search_FSM IS
	TYPE State_type IS (S1, S2, S3, S4);
		attribute syn_encoding : string;
		attribute syn_encoding of State_type : type is "00 01 10 11";
	signal y_Q,y_D : State_type;
	signal addr : std_logic_vector(7 downto 0) := "00010000";
	signal A, shift, q: std_logic_vector(7 downto 0);
	signal over : std_logic;
BEGIN

	ram: entity work.ram32x8(SYN)
		port map(addr(4 downto 0),clk,"00000000",'0',q);

	PROCESS (clk) is --state table
	BEGIN
		if rising_edge(clk) then
			if (en = '0') then
				y_D <= S1;
			else
				case y_Q IS
					when S1 => 
						if (start = '0') then y_D <= S1;
						else y_D <= S2;
						end if;
					when S2 => 
						if (A = q) then 
							y_D <= S3;
						elsif (q > A) then
							if (addr = 31) then y_D <= S4;
							else
								over <= '1';
								y_D <= S2;
							end if;
						elsif (q < A) then
							if (addr = 0) then y_D <= S4;
							else
								over <= '0';
								y_D <= S2;
							end if;
						else  y_D <= S2;
						end if;
					when S3 => --stato fine con successo
						if (start = '0') then y_D <= S1;
						else y_D <= S3;
						end if;
					when S4 => --stato fine con errore
						if (start = '0') then y_D <= S1;
						else y_D <= S4;
						end if;
				end case;
			end if;
		end if;
	END PROCESS;
	
	PROCESS (clk) is
	BEGIN
		if rising_edge(clk) then
			y_Q <= y_D;
			case y_Q IS
				when S1 => 
					A <= n_in;
					addr <= "00010000";
					shift <= "00100000";
					found <= '0';
				when S2 => 
					shift <= ('0' & shift(7 downto 1));
					if over = '1' then
						addr <= addr + shift;
					else 
						addr <= addr - shift;
					end if;
				when S3 => 
					found <= '1';
				when S4 => 
					found <= '0';
			end case;
		end if;
	END PROCESS;
	
	address <= addr;
END behavior;