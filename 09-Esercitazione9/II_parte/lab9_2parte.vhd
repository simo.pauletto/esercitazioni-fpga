LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab9_2parte IS
	
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab9_2parte;


ARCHITECTURE structural OF lab9_2parte IS
	signal address : std_logic_vector(7 downto 0);
BEGIN
	
	FSM: entity work.dicotomic_search_FSM(behavior)
		port map(CLOCK_50,KEY(0),SW(9),SW(7 downto 0),address,LEDR(9));
		
	address_2: entity work.binary_to_hex_encoder(behavior)
		port map(address(7 downto 4),HEX1);	
		
	address_1: entity work.binary_to_hex_encoder(behavior)
		port map(address(3 downto 0),HEX0);
	
END structural;