LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

ENTITY count_high_bits_FSM IS
	generic (n: natural);
	PORT (
		clk, en, start : in std_logic;
		n_in : in std_logic_vector(n-1 downto 0);
		count : out std_logic_vector(n-1 downto 0)
		);
	
END count_high_bits_FSM;


ARCHITECTURE behavior OF count_high_bits_FSM IS
	TYPE State_type IS (S1, S2, S3);
		attribute syn_encoding : string;
		attribute syn_encoding of State_type : type is "00 01 10";
	signal y_Q,y_D : State_type;
	signal A : std_logic_vector(n-1 downto 0);
	signal result : std_logic_vector(n-1 downto 0); 
BEGIN
	PROCESS (clk) is --state table
	BEGIN
		if rising_edge(clk) then
			if (en = '0') then
				y_D <= S1;
			else
				case y_Q IS
					when S1 => 
						if (start = '0') then y_D <= S1;
						else y_D <= S2;
						end if;
					when S2 => 
						if (A = 0) then y_D <= S3;
						else  y_D <= S2;
						end if;
					when S3 => 
						if (start = '0') then y_D <= S1;
						else y_D <= S3;
						end if;
				end case;
			end if;
		end if;
	END PROCESS;
	
	PROCESS (clk) is
	BEGIN
		if rising_edge(clk) then
			y_Q <= y_D;
			case y_Q IS
				when S1 => 
					-- Load A
					A <= n_in;
					result <= (others => '0');
				when S2 => 
					-- right shif A
					A <= ('0' & A(n-1 downto 1));
					if A(0)='1' then
						result <= result + 1;
					end if;
				when S3 => 
					-- done
			end case;
		end if;
	END PROCESS;
	
	count <= result;
	
END behavior;