LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab9_1parte IS
	
	PORT (
		SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		LEDR: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		
		HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
		
		KEY: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		CLOCK_50: IN STD_LOGIC
		);
	
END lab9_1parte;


ARCHITECTURE structural OF lab9_1parte IS
	signal count : std_logic_vector(7 downto 0);
BEGIN
		
	FSM: entity work.count_high_bits_FSM(behavior)
		generic map (8)
		port map(CLOCK_50,KEY(0),SW(9),SW(7 downto 0),count);
		
	display: entity work.binary_to_hex_encoder(behavior)
		port map(count(3 downto 0),HEX0);
	
END structural;