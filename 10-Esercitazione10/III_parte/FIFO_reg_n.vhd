LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY FIFO_reg_n IS
	GENERIC (n: natural; k:natural);
	PORT
	(
		clk, res, en: in std_logic; --sarebbe da aggiungere un reset
		data_in: in std_logic_vector(n-1 downto 0);
		data_out: out std_logic_vector(n-1 downto 0)
	);
END FIFO_reg_n;

ARCHITECTURE behavior OF FIFO_reg_n IS
	TYPE mem IS ARRAY(0 TO k-1) OF STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	SIGNAL data : mem;
BEGIN

	process(clk, res) is --sarebbe da aggiungere un reset
	begin
		if (res ='0') then 
			--data <= (OTHERS => '0');
			data_out <= (others => '0');
		elsif rising_edge(clk) then
			if(en = '1') then
				data(1 to k-1) <= data(0 to k-2);
				data(0) <= data_in;
				data_out <= data(k-1);
			else
				data_out <= (others => '0');
			end if;
		end if;
	end process;

END behavior;