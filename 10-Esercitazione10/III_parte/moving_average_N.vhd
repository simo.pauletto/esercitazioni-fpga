LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY moving_average_N IS
	GENERIC (n: natural);
   PORT ( 
		clk, res, en: in std_logic;
		data_in: in std_logic_vector(23 downto 0);
		data_out: out std_logic_vector(23 downto 0)
	);
END moving_average_N;

ARCHITECTURE Structural OF moving_average_N IS
	signal input_divided, input_past_n, buf_out, buf_out_past : std_logic_vector(23 downto 0);
BEGIN
	
	--input divided by 32
	input_divided <= ((data_in(23))&(data_in(23))&(data_in(23))&(data_in(23))&(data_in(23)))&(data_in(23 DOWNTO 5));
	
	fifo: entity work.FIFO_reg_n(behavior)
		generic map(24,n)
		port map(clk,res,en,input_divided,input_past_n);
		
	
	reg: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,buf_out,buf_out_past);
		
	buf_out <= buf_out_past + input_divided - input_past_n;
	data_out <= buf_out;
		
END Structural;
