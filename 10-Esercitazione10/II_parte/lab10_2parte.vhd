LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY lab10_2parte IS
   PORT ( CLOCK_50, CLOCK2_50, AUD_DACLRCK   : IN    STD_LOGIC;
          AUD_ADCLRCK, AUD_BCLK, AUD_ADCDAT  : IN    STD_LOGIC;
          KEY                                : IN    STD_LOGIC_VECTOR(0 DOWNTO 0);
          FPGA_I2C_SDAT                      : INOUT STD_LOGIC;
          FPGA_I2C_SCLK, AUD_DACDAT, AUD_XCK : OUT   STD_LOGIC;
			 SW											: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
			 LEDR											: OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
			 );
END lab10_2parte;

ARCHITECTURE Behavior OF lab10_2parte IS

   COMPONENT clock_generator
      PORT( CLOCK2_50 : IN STD_LOGIC;
            reset    : IN STD_LOGIC;
            AUD_XCK  : OUT STD_LOGIC);
   END COMPONENT;

   COMPONENT audio_and_video_config
      PORT( CLOCK_50, reset : IN    STD_LOGIC;
            I2C_SDAT        : INOUT STD_LOGIC;
            I2C_SCLK        : OUT   STD_LOGIC);
   END COMPONENT;   

   COMPONENT audio_codec
      PORT( CLOCK_50, reset, read_s, write_s               : IN  STD_LOGIC;
            writedata_left, writedata_right                : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK, AUD_DACLRCK : IN  STD_LOGIC;
            read_ready, write_ready                        : OUT STD_LOGIC;
            readdata_left, readdata_right                  : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_DACDAT                                     : OUT STD_LOGIC);
   END COMPONENT;

   SIGNAL read_ready, write_ready, read_s, write_s : STD_LOGIC;
   SIGNAL readdata_left, readdata_right            : STD_LOGIC_VECTOR(23 DOWNTO 0);
   SIGNAL writedata_left, writedata_right          : STD_LOGIC_VECTOR(23 DOWNTO 0);   
   SIGNAL reset                                    : STD_LOGIC;
	
	SIGNAL filt_data_left, filt_data_right : STD_LOGIC_VECTOR(23 downto 0);
	SIGNAL en 										: STD_LOGIC;
 
BEGIN
   reset <= NOT(KEY(0));

   --YOUR CODE GOES HERE
	
	en <= (read_ready AND write_ready);
	
   filter_right: entity work.moving_average_8(behavior)
		port map(en,'1','1',readdata_right,filt_data_right);
		
	filter_left: entity work.moving_average_8(behavior)
		port map(en,'1','1',readdata_left,filt_data_left);

		
	writedata_left <= readdata_left when ((SW(0)='0') and (en = '1')) else
							filt_data_left when ((SW(0) = '1') and (en = '1'));
	
	writedata_right <= readdata_right when ((SW(0) = '0') and (en = '1')) else
							filt_data_right when ((SW(0) = '1') and (en = '1'));
		
--	process (SW(0),en) is
--	begin
--		if (SW(0) = '0') then 
--			if rising_edge(en) then
--				writedata_left <= readdata_left; --when (read_ready='1' AND write_ready='1');
--				writedata_right <= readdata_right; --when (read_ready='1' AND write_ready='1');
--			end if;
--		else 
--			-- applicare filtro
--			if rising_edge(en) then
--				writedata_left <= filt_data_left; -- when (read_ready='1' AND write_ready='1');
--				writedata_right <= filt_data_right; -- when (read_ready='1' AND write_ready='1');
--			end if;
--		end if;
--	end process;
	
   read_s <= en;
	write_s <= en;
			
   my_clock_gen: clock_generator PORT MAP (CLOCK2_50, reset, AUD_XCK);
   cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
   codec: audio_codec PORT MAP (CLOCK_50, reset, read_s, write_s, writedata_left, 
	                             writedata_right, AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK,
										  AUD_DACLRCK, read_ready, write_ready, readdata_left, 
										  readdata_right, AUD_DACDAT);
										  
	
END Behavior;
