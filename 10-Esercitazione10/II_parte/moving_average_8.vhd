LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY moving_average_8 IS
   PORT ( 
		clk, res, en: in std_logic;
		sample_in: in std_logic_vector(23 downto 0);
		sample_out: out std_logic_vector(23 downto 0)
	);
END moving_average_8;

ARCHITECTURE Behavior OF moving_average_8 IS

signal reg1_out,reg2_out,reg3_out,reg4_out,reg5_out,reg6_out,reg7_out, sample_out_buf : std_logic_vector(23 downto 0);
signal sample_in_div, reg1_out_div, reg2_out_div, reg3_out_div, reg4_out_div,reg5_out_div,reg6_out_div,reg7_out_div : std_logic_vector(23 downto 0);
 
BEGIN
  
	reg1: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,sample_in,reg1_out);
		
	reg2: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg1_out,reg2_out);
		
	reg3: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg2_out,reg3_out);
		
	reg4: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg3_out,reg4_out);
		
	reg5: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg4_out,reg5_out);
		
	reg6: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg5_out,reg6_out);
		
	reg7: entity work.reg_nbit(behavior)
		generic map(24)
		port map(clk,res,en,reg6_out,reg7_out);
		
	sample_in_div <= (sample_in(23) & sample_in(23) & sample_in(23) & sample_in(23 downto 3));
	reg1_out_div <= (reg1_out(23) & reg1_out(23) & reg1_out(23) & reg1_out(23 downto 3));
	reg2_out_div <= (reg2_out(23) & reg2_out(23) & reg2_out(23) & reg2_out(23 downto 3));
	reg3_out_div <= (reg3_out(23) & reg3_out(23) & reg3_out(23) & reg3_out(23 downto 3));
	reg4_out_div <= (reg4_out(23) & reg4_out(23) & reg4_out(23) & reg4_out(23 downto 3));
	reg5_out_div <= (reg5_out(23) & reg5_out(23) & reg5_out(23) & reg5_out(23 downto 3));
	reg6_out_div <= (reg6_out(23) & reg6_out(23) & reg6_out(23) & reg6_out(23 downto 3));
	reg7_out_div <= (reg7_out(23) & reg7_out(23) & reg7_out(23) & reg7_out(23 downto 3));
  
	sample_out_buf <= sample_in_div + reg1_out_div + reg2_out_div + reg3_out_div + reg4_out_div + reg5_out_div + reg6_out_div + reg7_out_div;
	--volume adjust
	sample_out <= (sample_out_buf(23) & sample_out_buf(23) & sample_out_buf(23) & sample_out_buf(23 downto 3));
	
END Behavior;
