LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY lab10_1parte IS
   PORT ( CLOCK_50, CLOCK2_50, AUD_DACLRCK   : IN    STD_LOGIC;
          AUD_ADCLRCK, AUD_BCLK, AUD_ADCDAT  : IN    STD_LOGIC;
          KEY                                : IN    STD_LOGIC_VECTOR(0 DOWNTO 0);
          FPGA_I2C_SDAT                      : INOUT STD_LOGIC;
          FPGA_I2C_SCLK, AUD_DACDAT, AUD_XCK : OUT   STD_LOGIC);
END lab10_1parte;

ARCHITECTURE Behavior OF lab10_1parte IS

   COMPONENT clock_generator
      PORT( CLOCK2_50 : IN STD_LOGIC;
            reset    : IN STD_LOGIC;
            AUD_XCK  : OUT STD_LOGIC);
   END COMPONENT;

   COMPONENT audio_and_video_config
      PORT( CLOCK_50, reset : IN    STD_LOGIC;
            I2C_SDAT        : INOUT STD_LOGIC;
            I2C_SCLK        : OUT   STD_LOGIC);
   END COMPONENT;   

   COMPONENT audio_codec
      PORT( CLOCK_50, reset, read_s, write_s               : IN  STD_LOGIC;
            writedata_left, writedata_right                : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK, AUD_DACLRCK : IN  STD_LOGIC;
            read_ready, write_ready                        : OUT STD_LOGIC;
            readdata_left, readdata_right                  : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_DACDAT                                     : OUT STD_LOGIC);
   END COMPONENT;

   SIGNAL read_ready, write_ready, read_s, write_s : STD_LOGIC;
   SIGNAL readdata_left, readdata_right            : STD_LOGIC_VECTOR(23 DOWNTO 0);
   SIGNAL writedata_left, writedata_right          : STD_LOGIC_VECTOR(23 DOWNTO 0);   
   SIGNAL reset                                    : STD_LOGIC;
	
	SIGNAL buf_data_left, buf_data_right : STD_LOGIC_VECTOR(23 downto 0);
 
BEGIN
   reset <= NOT(KEY(0));

   --YOUR CODE GOES HERE
--   writedata_left <= ... not shown
--   writedata_right <= ... not shown
--   read_s <= ... not shown
--   write_s <= ... not shown

	writedata_left <= readdata_left when (read_ready='1' AND write_ready='1');
	writedata_right <= readdata_right when (read_ready='1' AND write_ready='1');
	read_s <= read_ready AND write_ready;
	write_s <= read_ready AND write_ready;
   
   my_clock_gen: clock_generator PORT MAP (CLOCK2_50, reset, AUD_XCK);
   cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
   codec: audio_codec PORT MAP (CLOCK_50, reset, read_s, write_s, writedata_left, 
	                             writedata_right, AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK,
										  AUD_DACLRCK, read_ready, write_ready, readdata_left, 
										  readdata_right, AUD_DACDAT);
										  
	
--	read_data_audio: process (read_ready) is
--	begin
--		if rising_edge(read_ready) then
--			read_s <= '1';
--			buf_data_left <= readdata_left;
--			buf_data_right <= readdata_right;
--			read_s <= '0';
--		end if;
--	end process;
--	
--	write_data_audio: process (write_ready) is
--	begin
--		if rising_edge(write_ready) then
--			
--			writedata_left <= buf_data_left;
--			writedata_right <= buf_data_right;
--			write_s <= '1';
--			write_s <= '0';
--		end if;
--	end process;
	
END Behavior;
