LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY binToHex_signed IS
	PORT (
		n_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		hex_display_sign : OUT STD_LOGIC_VECTOR(0 TO 6);
		hex_display_digit : OUT STD_LOGIC_VECTOR(0 TO 6)
		);
END binToHex_signed;


ARCHITECTURE structural OF binToHex_signed IS
signal n_show : std_logic_vector(3 downto 0);
BEGIN	
	
		hex_display_sign <= "1111110" when n_in(3)='1' else "1111111";
		
		n_show <= 	n_in when n_in(3) = '0' else
						std_logic_vector(signed(not(n_in)+1));
						
		digit: entity work.binary_to_hex_encoder(behavior)
			port map(n_show,hex_display_digit);
	
END structural;