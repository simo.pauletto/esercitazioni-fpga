LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY multiplexer_21_n IS
	GENERIC(n : natural := 24);
	PORT (
		sel : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		n_in_0,n_in_1,n_in_2,n_in_3,n_in_4,n_in_5,n_in_6,n_in_7,n_in_8,n_in_9,n_in_10,n_in_11,n_in_12,n_in_13,n_in_14,n_in_15,n_in_16,n_in_17,n_in_18 : IN STD_LOGIC_VECTOR(n-1 downto 0) := (others => '0');
		n_out : OUT STD_LOGIC_VECTOR(n-1 downto 0)
		);
	END multiplexer_21_n; 

ARCHITECTURE Behavior OF multiplexer_21_n IS
	BEGIN
	
	n_out <= 
		n_in_0 when sel = 1 else
		n_in_1 when sel = 2 else
		n_in_2 when sel = 3 else
		n_in_3 when sel = 4 else
		n_in_4 when sel = 5 else
		n_in_5 when sel = 6 else
		n_in_6 when sel = 7 else
		n_in_7 when sel = 8 else
		n_in_8 when sel = 9 else
		n_in_9 when sel = 10 else
		n_in_10 when sel = 11 else
		n_in_11 when sel = 12 else
		n_in_12 when sel = 13 else
		n_in_13 when sel = 14 else
		n_in_14 when sel = 15 else
		n_in_15 when sel = 16 else
		n_in_16 when sel = 17 else
		n_in_17 when sel = 18 else
		n_in_18 when sel = 19 else
		n_in_18;
	
	END Behavior;