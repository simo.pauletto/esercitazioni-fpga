LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY coeff_selection_mux IS
	PORT (
		sel : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		n_out : OUT STD_LOGIC_VECTOR(35 DOWNTO 0)
		);
	END coeff_selection_mux; 

ARCHITECTURE Behavior OF coeff_selection_mux IS

	CONSTANT zero : std_logic_vector(35 downto 0) := (others => '0');

	CONSTANT k1_n	: std_logic_vector(35 downto 0) := "011111111110110011100100001100101110"; 				-- Q1.35: 0.999416851857574	->	0.11111111110110011100100001100101110
	CONSTANT k1		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k1_n) +1);					-- Q1.35: -0.999416851857574 	-> neg(k1_n)
	
	CONSTANT k2		: std_logic_vector(35 downto 0) := "011111111110100000100111001110111001"; 				-- Q1.35: 0.999272255047943	-> 0.11111111110100000100111001110111001
	CONSTANT k2_n	: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k2) +1); 					-- Q1.35: -0.999272255047943	-> neg(k2)
	
	CONSTANT k3_n	: std_logic_vector(35 downto 0) := "011111100010011100010110101111101100"; 				-- Q1.35: 0.985567896989703	-> 0.11111100010011100010110101111101100
	CONSTANT k3		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k3_n) +1); 					-- Q1.35: -0.985567896989703	-> neg(k3_n)
	
	CONSTANT k4		: std_logic_vector(35 downto 0) := "011110010000101100010110110111111001"; 				-- Q1.35: 0.945650920079072	-> 0.11110010000101100010110110111111001
	CONSTANT k4_n	: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k4) +1);						-- Q1.35: -0.945650920079072	-> neg(k4)
	
	CONSTANT k5_n	: std_logic_vector(35 downto 0) := "011101011101001101111110011011011001"; 				-- Q1.35: 0.920516780397487	-> 0.11101011101001101111110011011011001
	CONSTANT k5		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k5_n) +1); 					-- Q1.35: -0.920516780397487	-> neg(k5_n)
	
	CONSTANT k6		: std_logic_vector(35 downto 0) := "010011011000000011011100100010000011";					-- Q1.35: 0.605495039466374	-> 0.10011011000000011011100100010000011
	CONSTANT k6_n	: std_logic_vector(35 downto 0) := std_logic_vector(signed(not k6) +1); 					-- Q1.35: -0.605495039466374	-> neg(k6)
	
	
	
	CONSTANT v1_n	: std_logic_vector(35 downto 0) := "000000000000000000000010110001001001";
	CONSTANT v1		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not v1_n)+1);					-- Q1.35: -0.000000329978027974881	-> neg(0.00000000000000000000010110001001001)
	
	CONSTANT v2		: std_logic_vector(35 downto 0) := "000000000000001011111111000110110000"; 				-- Q1.35: 0.0000914461041319203		-> 0.00000000000001011111111000110110000
	
	CONSTANT v3		: std_logic_vector(35 downto 0) := "000000000100010101010001011100000110"; 				-- Q1.35: 0.00211542119264719			-> 0.00000000100010101010001011100000110
	
	CONSTANT v4_n	: std_logic_vector(35 downto 0) := "000000111100101001100000010000001000";
	CONSTANT v4		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not v4_n)+1); 					-- Q1.35: -0.0296135249184840			-> neg(000000111100101001100000010000001000)
	
	CONSTANT v5_n	: std_logic_vector(35 downto 0) := "000001011110010010100100101110101000";
	CONSTANT v5		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not v5_n)+1); 					-- Q1.35: -0.0460401450087405			-> neg(0.00001011110010010100100101110101000)
	
	CONSTANT v6_n	: std_logic_vector(35 downto 0) := "000001011000100111001111111010111100";
	CONSTANT v6		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not v6_n)+1); 					-- Q1.35: -0.0432681943274923			-> neg(0.00001011000100111001111111010111100)
	
	CONSTANT v7_n	: std_logic_vector(35 downto 0) := "000000100011000101000011000100100010";
	CONSTANT v7		: std_logic_vector(35 downto 0) := std_logic_vector(signed(not v7_n)+1);					-- Q1.35: -0.0171283567985286			-> neg(0.00000100011000101000011000100100010)
	
	
---------------------------------------------------------------------------------------------------------------------------
--											COEFFICIENTS IN FIXED POINT FORMAT Q1.23
---------------------------------------------------------------------------------------------------------------------------
--	CONSTANT zero : std_logic_vector(23 downto 0) := (others => '0');
--
--	CONSTANT k1_n	: std_logic_vector(23 downto 0) := "011111111110110011100100"; 				-- Q1.23: 0.999416851857574	->	0.111111111101100111001000
--	CONSTANT k1		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k1_n) +1);	-- Q1.23: -0.999416851857574 	-> neg(k1_n)
--	
--	CONSTANT k2		: std_logic_vector(23 downto 0) := "011111111110100000100111"; 				-- Q1.23: 0.999272255047943	-> 0.111111111101000001001110
--	CONSTANT k2_n	: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k2) +1); 	-- Q1.23: -0.999272255047943	-> neg(k2)
--	
--	CONSTANT k3_n	: std_logic_vector(23 downto 0) := "011111100010011100010110"; 				-- Q1.23: 0.985567896989703	-> 0.111111000100111000101101
--	CONSTANT k3		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k3_n) +1); 	-- Q1.23: -0.985567896989703	-> neg(k3_n)
--	
--	CONSTANT k4		: std_logic_vector(23 downto 0) := "011110010000101100010110"; 				-- Q1.23: 0.945650920079072	-> 0.111100100001011000101101
--	CONSTANT k4_n	: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k4) +1);		-- Q1.23: -0.945650920079072	-> neg(k4)
--	
--	CONSTANT k5_n	: std_logic_vector(23 downto 0) := "011101011101001101111110"; 				-- Q1.23: 0.920516780397487	-> 0.111010111010011011111100
--	CONSTANT k5		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k5_n) +1); 	-- Q1.23: -0.920516780397487	-> neg(k5_n)
--	
--	CONSTANT k6		: std_logic_vector(23 downto 0) := "010011011000000011011100";					-- Q1.23: 0.605495039466374	-> 0.100110110000000110111001
--	CONSTANT k6_n	: std_logic_vector(23 downto 0) := std_logic_vector(signed(not k6) +1); 	-- Q1.23: -0.605495039466374	-> neg(k6)
--	
--	
--	
--	CONSTANT v1_n	: std_logic_vector(23 downto 0) := "000000000000000000000010";
--	CONSTANT v1		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not v1_n)+1);	-- Q1.23: -0.000000329978027974881	-> neg(0.000000000000000000000101)
--	
--	CONSTANT v2		: std_logic_vector(23 downto 0) := "000000000000001011111111"; 				-- Q1.23: 0.0000914461041319203		-> 0.000000000000010111111110
--	
--	CONSTANT v3		: std_logic_vector(23 downto 0) := "000000000100010101010001"; 				-- Q1.23: 0.00211542119264719			-> 0.000000001000101010100010
--	
--	CONSTANT v4_n	: std_logic_vector(23 downto 0) := "000000111100101001100000";
--	CONSTANT v4		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not v4_n)+1); 	-- Q1.23: -0.0296135249184840			-> neg(0.000001111001010011000000)
--	
--	CONSTANT v5_n	: std_logic_vector(23 downto 0) := "000001011110010010100100";
--	CONSTANT v5		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not v5_n)+1); 	-- Q1.23: -0.0460401450087405			-> neg(0.000010111100100101001001)
--	
--	CONSTANT v6_n	: std_logic_vector(23 downto 0) := "000001011000100111001111";
--	CONSTANT v6		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not v6_n)+1); 	-- Q1.23: -0.0432681943274923			-> neg(0.000010110001001110011111)
--	
--	CONSTANT v7_n	: std_logic_vector(23 downto 0) := "000000100011000101000011";
--	CONSTANT v7		: std_logic_vector(23 downto 0) := std_logic_vector(signed(not v7_n)+1);	-- Q1.23: -0.0171283567985286			-> neg(0.000001000110001010000110)
	
	BEGIN
	
	n_out <= 
		k6_n when sel = 1 else
		k5_n when sel = 2 else
		k4_n when sel = 3 else
		k3_n when sel = 4 else
		k2_n when sel = 5 else
		k1_n when sel = 6 else
		k1 when sel = 7 else
		k2 when sel = 8 else
		k3 when sel = 9 else
		k4 when sel = 10 else
		k5 when sel = 11 else
		k6 when sel = 12 else
		v1 when sel = 13 else
		v2 when sel = 14 else
		v3 when sel = 15 else
		v4 when sel = 16 else
		v5 when sel = 17 else
		v6 when sel = 18 else
		v7 when sel = 19 else
		v7; 
	
	END Behavior;