LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY Filter_tb IS
END Filter_tb;

ARCHITECTURE bench OF Filter_tb IS
	SIGNAL Clock, Enable, Reset : STD_LOGIC := '0';
	SIGNAL DataIn: STD_LOGIC_VECTOR(23 DOWNTO 0) := "011111111111111111111111";  --Q1.23
	SIGNAL DataOut: STD_LOGIC_VECTOR(23 DOWNTO 0) := "000000000000000000000000";  --Q1.23
	SIGNAL op_count: STD_LOGIC_VECTOR(4 DOWNTO 0) := "00000";
	CONSTANT Tpw_clk : time := 10ns;

BEGIN

	DUT : ENTITY work.single_channel_filter
		PORT MAP (clock,Enable,'1',op_count,DataIn,DataOut);

	counter: entity work.operation_counter
		GENERIC MAP(0,20,5)
		PORT MAP(Enable,clock,'1','1',op_count);

	clock_gen : process is
	begin
		Clock <= '1' after Tpw_clk, '0' after 2 * Tpw_clk;
		wait for 2 * Tpw_clk;
	end process clock_gen;														 
															 
	stimulus : process is
	begin
		Enable <= '0';  wait for 1 us;
		Enable <= '1';  wait for 20 ns;
		Enable <= '0';  wait for 1 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 1 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 1 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';
		wait;
	end process stimulus;
	
END ARCHITECTURE;
