LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY single_channel_filter IS
	PORT ( 
		clk, en, reset : in std_logic;
		op_count : in std_logic_vector(4 downto 0);
		sample_in : in std_logic_vector(23 downto 0);
		sample_out : out std_logic_vector(23 downto 0)
		);
END ENTITY;

ARCHITECTURE Structural OF single_channel_filter IS
	constant zero : std_logic_vector(35 downto 0) := (others => '0');
	
	signal e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18,e19 : std_logic;
	
	signal f5,f4,f3,f2,f1,f0,g1,g2,g3,g4,g5,g6,v1,v2,v3,v4,v5,v6,v7: std_logic_vector(35 downto 0);
	signal f0_past,g5_past,g4_past,g3_past,g2_past,g1_past: std_logic_vector(35 downto 0);
	
	signal coeff : std_logic_vector(35 downto 0);
	signal sample_in_extended,sample_out_extended,past_value,temp_addend_1_crop,temp_addend_2,temp_out,sum: std_logic_vector(35 downto 0);
	signal st1,st2,st3,stt1,stt2,vt1,vt2,vt3,vtt1,vtt2,sum_out: std_logic_vector(35 downto 0);
	signal temp_addend_1 : std_logic_vector(71 downto 0);
	
BEGIN
	
	-- counter for operations 	->	mux for coefficents ->	multiplier
	--									-> mux for past values ->
	
	coeff_mux: entity work.coeff_selection_mux(behavior)
		port map(op_count, coeff);
		
	addend1_mux: entity work.multiplexer_21_n(behavior)
		generic map(36)
		port map(op_count, g5_past, g4_past, g3_past, g2_past, g1_past, f0_past, f0, f1, f2, f3, f4, f5, f0, g1, g2, g3, g4, g5, g6, past_value);

	temp_addend_1 <= std_logic_vector(signed(coeff) * signed(past_value));
	-- clipping Q18.18 (same per Q13.23)
	temp_addend_1_crop <= temp_addend_1(70 downto 35) when temp_addend_1(71) = temp_addend_1(70) else (35 => temp_addend_1(71), others => not temp_addend_1(71));
	
	-- Q18.18
	sample_in_extended <= std_logic_vector(resize(signed(sample_in(23 downto 5)),36));
	
	-- Q13.26
	-- sample_in_extended <= std_logic_vector(resize(signed(sample_in),36));
	
	addend2_mux: entity work.multiplexer_21_n(behavior)
		generic map(36)
		port map(op_count, sample_in_extended, f5, f4, f3, f2, f1, f0_past, g1_past, g2_past, g3_past, g4_past, g5_past, zero, zero, zero, zero, zero, zero, zero, temp_addend_2);
		
	sum <= std_logic_vector(signed(temp_addend_2) + signed(temp_addend_1_crop));
	--clipping
	temp_out <= (35 => temp_addend_1_crop(35), others => not temp_addend_1_crop(35)) when ((temp_addend_1_crop(35) = temp_addend_2(35)) and (sum(35) /= temp_addend_1_crop(35))) else sum;

	-- enable signals based on counter for operations
	e1 <= '1' when (op_count = 1) else '0';
	e2 <= '1' when (op_count = 2) else '0';
	e3 <= '1' when (op_count = 3) else '0';
	e4 <= '1' when (op_count = 4) else '0';
	e5 <= '1' when (op_count = 5) else '0';
	e6 <= '1' when (op_count = 6) else '0';
	e7 <= '1' when (op_count = 7) else '0';
	e8 <= '1' when (op_count = 8) else '0';
	e9 <= '1' when (op_count = 9) else '0';
	e10 <= '1' when (op_count = 10) else '0';
	e11 <= '1' when (op_count = 11) else '0';
	e12 <= '1' when (op_count = 12) else '0';
	e13 <= '1' when (op_count = 13) else '0';
	e14 <= '1' when (op_count = 14) else '0';
	e15 <= '1' when (op_count = 15) else '0';
	e16 <= '1' when (op_count = 16) else '0';
	e17 <= '1' when (op_count = 17) else '0';
	e18 <= '1' when (op_count = 18) else '0';
	e19 <= '1' when (op_count = 19) else '0';
		
	--first level registers
	reg1 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e1,reset,temp_out,f5);
		
	reg2 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e2,reset,temp_out,f4);
		
	reg3 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e3,reset,temp_out,f3);
		
	reg4 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e4,reset,temp_out,f2);
		
	reg5 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e5,reset,temp_out,f1);
		
	reg6 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e6,reset,temp_out,f0);
		
		
	
	reg7 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e7,reset,temp_out,g1);
		
	reg8 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e8,reset,temp_out,g2);
		
	reg9 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e9,reset,temp_out,g3);
		
	reg10 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e10,reset,temp_out,g4);
		
	reg11 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e11,reset,temp_out,g5);
		
	reg12 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e12,reset,temp_out,g6);
		
	
	
	reg13 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e13,reset,temp_out,v1);
		
	reg14 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e14,reset,temp_out,v2);
		
	reg15 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e15,reset,temp_out,v3);
		
	reg16 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e16,reset,temp_out,v4);
		
	reg17 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e17,reset,temp_out,v5);
		
	reg18 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e18,reset,temp_out,v6);
		
	reg19 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e19,reset,temp_out,v7);
		
	
	
	-- second level register (past values)
	reg_q1_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,f0,f0_past);
		
	reg_p1_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,g1,g1_past);
		
	reg_p2_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,g2,g2_past);
		
	reg_p3_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,g3,g3_past);
		
	reg_p4_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,g4,g4_past);
		
	reg_p5_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,g5,g5_past);
		
	
	-- final sum and clipping for sum overflow
	st1 <= v1 + v2;
	vt1 <= (35 => v1(35), others => not v1(35)) when v1(35) = v2(35) and st1(35)/=v1(35) else st1;
	
	st2 <= v3 + v4;
	vt2 <= (35 => v3(35), others => not v3(35)) when v3(35) = v4(35) and st2(35)/=v3(35) else st2;
	
	st3 <= v5 + v6;
	vt3 <= (35 => v5(35), others => not v5(35)) when v5(35) = v6(35) and st3(35)/=v5(35) else st3;
	
	stt2 <= vt3 + v7;
	vtt2 <= (35 => vt3(35), others => not vt3(35)) when vt3(35) = v7(35) and stt2(35)/=vt3(35) else stt2;
	
	stt1 <= vt1 + vt2;
	vtt1 <= (35 => vt1(35), others => not vt1(35)) when vt1(35) = vt2(35) and stt1(35)/=vt1(35) else stt1;
	
	sum_out <= vtt1 + vtt2;
	sample_out_extended <= (35 => vtt1(35), others => not vtt1(35)) when vtt1(35) = vtt2(35) and sum_out(35)/=vtt1(35) else sum_out;
	
	-- clipping for fixed point Q13.23
--	sample_out <= sample_out_extended(23 downto 0) when 	sample_out_extended(35) = sample_out_extended(34) and
--																			sample_out_extended(35) = sample_out_extended(33) and
--																			sample_out_extended(35) = sample_out_extended(32) and
--																			sample_out_extended(35) = sample_out_extended(31) and
--																			sample_out_extended(35) = sample_out_extended(30) and
--																			sample_out_extended(35) = sample_out_extended(29) and
--																			sample_out_extended(35) = sample_out_extended(28) and
--																			sample_out_extended(35) = sample_out_extended(27) and
--																			sample_out_extended(35) = sample_out_extended(26) and
--																			sample_out_extended(35) = sample_out_extended(25) and
--																			sample_out_extended(35) = sample_out_extended(24) and
--																			sample_out_extended(35) = sample_out_extended(23) else
--																			(23 => sample_out_extended(35), others => not sample_out_extended(35));
	
	-- clipping for fixed point conversion Q18.18
	sample_out <= sample_out_extended(18 downto 0) & '0' & '0' & '0' & '0' & '0' when 	sample_out_extended(35) = sample_out_extended(34) and
																													sample_out_extended(35) = sample_out_extended(33) and
																													sample_out_extended(35) = sample_out_extended(32) and
																													sample_out_extended(35) = sample_out_extended(31) and
																													sample_out_extended(35) = sample_out_extended(30) and
																													sample_out_extended(35) = sample_out_extended(29) and
																													sample_out_extended(35) = sample_out_extended(28) and
																													sample_out_extended(35) = sample_out_extended(27) and
																													sample_out_extended(35) = sample_out_extended(26) and
																													sample_out_extended(35) = sample_out_extended(25) and
																													sample_out_extended(35) = sample_out_extended(24) and
																													sample_out_extended(35) = sample_out_extended(23) and
																													sample_out_extended(35) = sample_out_extended(22) and
																													sample_out_extended(35) = sample_out_extended(21) and
																													sample_out_extended(35) = sample_out_extended(20) and
																													sample_out_extended(35) = sample_out_extended(19) and
																													sample_out_extended(35) = sample_out_extended(18) else
																													(23 => sample_out_extended(35), others => not sample_out_extended(35));
--	
	
END Structural;