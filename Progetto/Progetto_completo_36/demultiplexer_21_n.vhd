LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY demultiplexer_21_n IS
	GENERIC (n : natural := 24);
	PORT (
		sel : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		n_in : IN STD_LOGIC_VECTOR(n-1 DOWNTO 0);
		n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19 : OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0)
		);
	END demultiplexer_21_n; 

ARCHITECTURE Behavior OF demultiplexer_21_n IS
	BEGIN
	
	n1 <= n_in when sel = 1 else (others => 'Z');
	n2 <= n_in when sel = 2 else (others => 'Z');
	n3 <= n_in when sel = 3 else (others => 'Z');
	n4 <= n_in when sel = 4 else (others => 'Z');
	n5 <= n_in when sel = 5 else (others => 'Z');
	n6 <= n_in when sel = 6 else (others => 'Z');
	n7 <= n_in when sel = 7 else (others => 'Z');
	n8 <= n_in when sel = 8 else (others => 'Z');
	n9 <= n_in when sel = 9 else (others => 'Z');
	n10 <= n_in when sel = 10 else (others => 'Z');
	n11 <= n_in when sel = 11 else (others => 'Z');
	n12 <= n_in when sel = 12 else (others => 'Z');
	n13 <= n_in when sel = 13 else (others => 'Z');
	n14 <= n_in when sel = 14 else (others => 'Z');
	n15 <= n_in when sel = 15 else (others => 'Z');
	n16 <= n_in when sel = 16 else (others => 'Z');
	n17 <= n_in when sel = 17 else (others => 'Z');
	n18 <= n_in when sel = 18 else (others => 'Z');
	n19 <= n_in when sel = 19 else (others => 'Z');
	
	END Behavior;