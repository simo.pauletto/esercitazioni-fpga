LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY elliptic_filter_6order IS
   PORT ( CLOCK_50, CLOCK2_50, AUD_DACLRCK   : IN    STD_LOGIC;
          AUD_ADCLRCK, AUD_BCLK, AUD_ADCDAT  : IN    STD_LOGIC;
          KEY                                : IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
          FPGA_I2C_SDAT                      : INOUT STD_LOGIC;
          FPGA_I2C_SCLK, AUD_DACDAT, AUD_XCK : OUT   STD_LOGIC;
			 SW											: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
			 LEDR											: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
			 
			 HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
			 HEX1: OUT STD_LOGIC_VECTOR(0 TO 6)
			 );
END elliptic_filter_6order;

ARCHITECTURE Behavior OF elliptic_filter_6order IS

   COMPONENT clock_generator
      PORT( CLOCK2_50 : IN STD_LOGIC;
            reset    : IN STD_LOGIC;
            AUD_XCK  : OUT STD_LOGIC);
   END COMPONENT;

   COMPONENT audio_and_video_config
      PORT( CLOCK_50, reset : IN    STD_LOGIC;
            I2C_SDAT        : INOUT STD_LOGIC;
            I2C_SCLK        : OUT   STD_LOGIC);
   END COMPONENT;   

   COMPONENT audio_codec
      PORT( CLOCK_50, reset, read_s, write_s               : IN  STD_LOGIC;
            writedata_left, writedata_right                : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK, AUD_DACLRCK : IN  STD_LOGIC;
            read_ready, write_ready                        : OUT STD_LOGIC;
            readdata_left, readdata_right                  : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_DACDAT                                     : OUT STD_LOGIC);
   END COMPONENT;

   SIGNAL read_ready, write_ready, read_s, write_s : STD_LOGIC;
   SIGNAL readdata_left, readdata_right            : STD_LOGIC_VECTOR(23 DOWNTO 0);
   SIGNAL writedata_left, writedata_right          : STD_LOGIC_VECTOR(23 DOWNTO 0);   
   SIGNAL reset, reset_neg, clk_key1               : STD_LOGIC;
	
	SIGNAL filt_data_left, filt_data_right, buf_temp : STD_LOGIC_VECTOR(23 downto 0);
	SIGNAL en	  							 : STD_LOGIC;
	
	signal click_count : std_logic_vector(3 downto 0);
	signal op_count: std_logic_vector(4 downto 0);
	signal buf_cropped,volume_factor : std_logic_vector(23 downto 0);
	signal buffer_amplified : std_logic_vector(47 downto 0);
 
BEGIN
   reset <= not(KEY(0));
	reset_neg <= not(reset);
	clk_key1 <= not(KEY(1));
	
	en <= (read_ready AND write_ready);
	
	
	--conter for operations
	operations_counter: entity work.operation_counter(behavior)
		generic map(0,20,5)
		port map(en,CLOCK_50,'1',reset_neg,op_count);
		
		
	
	-- filters (one filter per channel)
	filter_left: entity work.single_channel_filter(structural)
		port map(CLOCK_50,en,reset_neg,op_count,readdata_left,filt_data_left);
		
	filter_right: entity work.single_channel_filter(structural)
		port map(CLOCK_50,en,reset_neg,op_count,readdata_right,filt_data_right);

	
	-- counter with KEY(1) 	-> display logic
	--								-> volume factor selector
	click_counter: entity work.counter(behavior)
		generic map(-4,4,4)
		port map(clk_key1,'1',reset_neg,click_count);
		
	display_volume: entity work.binToHex_signed(structural)
		port map(click_count,HEX1,HEX0);
		
	vol_selector: entity work.volume_factor_selector(behavior)
		port map(click_count,volume_factor);
	
	
	
	-- filter out -> channel selector -> multiplier
	ch_selection: entity work.channel_selector_in(behavior)
		port map(CLOCK_50,en,'1',SW(0),op_count,filt_data_left,filt_data_right,readdata_left, readdata_right,buf_temp);
		
		
	-- volume multiplier and clipping control	
	buffer_amplified <= std_logic_vector(signed(volume_factor) * signed(buf_temp));	
	buf_cropped <= buffer_amplified(43 downto 20) when	buffer_amplified(47) = buffer_amplified(46) and
																		buffer_amplified(47) = buffer_amplified(45) and
																		buffer_amplified(47) = buffer_amplified(44) and
																		buffer_amplified(47) = buffer_amplified(43) else
																		(23 => buffer_amplified(47),others => not buffer_amplified(47));
																		
	
	out_ch_selection: entity work.channel_selector_out(behavior)
		port map(CLOCK_50,'1',reset_neg,op_count,buf_cropped,writedata_left, writedata_right);
	
	
   read_s <= en;
	write_s <= en;
			
   my_clock_gen: clock_generator PORT MAP (CLOCK2_50, reset, AUD_XCK);
   cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
   codec: audio_codec PORT MAP (CLOCK_50, reset, read_s, write_s, writedata_left, 
	                             writedata_right, AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK,
										  AUD_DACLRCK, read_ready, write_ready, readdata_left, 
										  readdata_right, AUD_DACDAT);
										  
	
END Behavior;
