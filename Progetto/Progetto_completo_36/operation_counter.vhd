LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;


ENTITY operation_counter IS
	GENERIC (n_start : integer; n_stop : integer; k : natural);
	PORT ( 
		start_op, clk,en,res : in std_logic;
		n_out : out std_logic_vector(k-1 downto 0) := (others => '0');
		execution : out std_logic
		);
END ENTITY;

ARCHITECTURE Behavior OF operation_counter IS
	signal count, count_clock : integer := 0;
	signal execute : std_logic;
BEGIN
	
	process (start_op, clk, en, res) is
	begin
		if (res = '0') then
			count <= 0;
		elsif (rising_edge(clk) and start_op = '1') then
			execute <= '1';
			count <= n_start;
		elsif (rising_edge(clk) and en = '1' and execute = '1') then
			count_clock <= count_clock + 1;
			if (count_clock = 1) then
				count_clock <= 0;
				count <= count + 1;
				if (count = (n_stop+1)) then
					count <= n_start;
					execute <= '0';
				end if;
			end if;
		end if;	
	end process;
	
	n_out <= std_logic_vector(to_unsigned(count,k));
	execution <= execute;
	
END Behavior;