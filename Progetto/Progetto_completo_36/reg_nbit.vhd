LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY reg_nbit IS
	GENERIC (n: natural);
	PORT ( 
		Clk, en, res: IN STD_LOGIC;
		D: IN STD_LOGIC_VECTOR(n-1 DOWNTO 0);
		Q: OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0):= (others => '0')
		);
END reg_nbit;

ARCHITECTURE behavior OF reg_nbit IS
	SIGNAL out_internal : STD_LOGIC_VECTOR(n-1 DOWNTO 0):= (others => '0');
BEGIN
	process(clk, res) is
	begin
		if (res = '0') then
			out_internal <= (others => '0');
		elsif (rising_edge(clk) and (en='1')) then
			out_internal <= D;
		end if;
	end process;
	
	Q <= out_internal;
	
END behavior;