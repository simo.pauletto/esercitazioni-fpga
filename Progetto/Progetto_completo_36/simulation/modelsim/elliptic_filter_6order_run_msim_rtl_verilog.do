transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/clock_generator.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/audio_codec.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/audio_and_video_config.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_SYNC_FIFO.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_Slow_Clock_Generator.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_I2C_AV_Auto_Initialize.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_I2C.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_Clock_Edge.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_Audio_Out_Serializer.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_Audio_In_Deserializer.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/Altera_UP_Audio_Bit_Counter.v}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/reg_nbit.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/volume_factor_selector.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/operation_counter.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/elliptic_filter_6order.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/counter.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/channel_selector_out.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/binToHex_signed.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/binary_to_hex_encoder.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/channel_selector_in.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/multiplexer_21_n.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/coeff_selection_mux.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/single_channel_filter.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_36/enable_divider_2pulse.vhd}

