LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY single_channel_filter IS
	PORT ( 
		clk, en, reset : in std_logic;
		op_count : in std_logic_vector(4 downto 0);
		sample_in : in std_logic_vector(23 downto 0);
		sample_out : out std_logic_vector(23 downto 0)
		);
END ENTITY;

ARCHITECTURE Structural OF single_channel_filter IS
	constant zero : std_logic_vector(35 downto 0) := (others => '0');
	
	signal e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18,e19,en_fn_reg,en_accum : std_logic := '0';
	signal e1_2,e2_2,e3_2,e4_2,e5_2,e6_2,e7_2,e8_2,e9_2,e10_2,e11_2,e12_2,e13_2,e14_2,e15_2,e16_2,e17_2,e18_2,e19_2 : std_logic := '0';
	signal reset_accum : std_logic := '1';
	
	signal fn,f0,f0_past,g6_past,g5_past,g4_past,g3_past,g2_past,g1_past: std_logic_vector(35 downto 0);
	signal sum_accum_unclipped, sum_accum_clipped, sum_accum_buffered : std_logic_vector(35 downto 0);
	
	signal coeff : std_logic_vector(35 downto 0);
	signal sample_in_extended,sample_out_extended,past_value,temp_addend_1_crop,temp_addend_2,temp_out,sum: std_logic_vector(35 downto 0);
	signal st1,st2,st3,stt1,stt2,vt1,vt2,vt3,vtt1,vtt2,sum_out: std_logic_vector(35 downto 0);
	signal temp_addend_1 : std_logic_vector(71 downto 0);
	
BEGIN
	
	-- counter for operations 	->	mux for coefficents ->	multiplier
	--									-> mux for past values ->
	
	coeff_mux: entity work.coeff_selection_mux(behavior)
		port map(op_count, coeff);
		
	addend1_mux: entity work.multiplexer_21_n(behavior)
		generic map(36)
		port map(op_count, g5_past, fn, g4_past, fn, g3_past, fn, g2_past, fn, g1_past, fn, f0_past, f0, f0, g1_past, g2_past, g3_past, g4_past, g5_past, g6_past, past_value);

	temp_addend_1 <= std_logic_vector(signed(coeff) * signed(past_value));
	-- clipping Q18.18 (same per Q13.23)
	temp_addend_1_crop <= temp_addend_1(70 downto 35) when temp_addend_1(71) = temp_addend_1(70) else (35 => temp_addend_1(71), others => not temp_addend_1(71));
	
	-- Q18.18
	sample_in_extended <= std_logic_vector(resize(signed(sample_in(23 downto 5)),36));
	
	-- Q13.26
	-- sample_in_extended <= std_logic_vector(resize(signed(sample_in),36));
	
	addend2_mux: entity work.multiplexer_21_n(behavior)
		generic map(36)
		port map(op_count, sample_in_extended, g5_past, fn, g4_past, fn, g3_past, fn, g2_past, fn, g1_past, fn, f0_past, zero, zero, zero, zero, zero, zero, zero, temp_addend_2);
		
	sum <= std_logic_vector(signed(temp_addend_2) + signed(temp_addend_1_crop));
	--clipping
	temp_out <= (35 => temp_addend_1_crop(35), others => not temp_addend_1_crop(35)) when ((temp_addend_1_crop(35) = temp_addend_2(35)) and (sum(35) /= temp_addend_1_crop(35))) else sum;

	-- enable signals based on counter for operations
	e1 <= '1' when (op_count = 1) else '0';
	div_e1 : entity work.enable_divider_2pulse(behavior)
		generic map(1)
		port map(clk,e1,op_count,e1_2);
	
	
	e2 <= '1' when (op_count = 2) else '0';
	div_e2 : entity work.enable_divider_2pulse(behavior)
		generic map(2)
		port map(clk,e2,op_count,e2_2);
	
	e3 <= '1' when (op_count = 3) else '0';
	div_e3 : entity work.enable_divider_2pulse(behavior)
		generic map(3)
		port map(clk,e3,op_count,e3_2);

	
	e4 <= '1' when (op_count = 4) else '0';
	div_e4 : entity work.enable_divider_2pulse(behavior)
		generic map(4)
		port map(clk,e4,op_count,e4_2);
	
	
	e5 <= '1' when (op_count = 5) else '0';
	div_e5 : entity work.enable_divider_2pulse(behavior)
		generic map(5)
		port map(clk,e5,op_count,e5_2);
	
	
	e6 <= '1' when (op_count = 6) else '0';
	div_e6 : entity work.enable_divider_2pulse(behavior)
		generic map(6)
		port map(clk,e6,op_count,e6_2);
	
	
	e7 <= '1' when (op_count = 7) else '0';
	div_e7 : entity work.enable_divider_2pulse(behavior)
		generic map(7)
		port map(clk,e7,op_count,e7_2);
	
	
	e8 <= '1' when (op_count = 8) else '0';
	div_e8 : entity work.enable_divider_2pulse(behavior)
		generic map(8)
		port map(clk,e8,op_count,e8_2);
	
	
	e9 <= '1' when (op_count = 9) else '0';
	div_e9 : entity work.enable_divider_2pulse(behavior)
		generic map(9)
		port map(clk,e9,op_count,e9_2);

	
	e10 <= '1' when (op_count = 10) else '0';
	div_e10 : entity work.enable_divider_2pulse(behavior)
		generic map(10)
		port map(clk,e10,op_count,e10_2);
	
	
	e11 <= '1' when (op_count = 11) else '0';
	div_e11 : entity work.enable_divider_2pulse(behavior)
		generic map(11)
		port map(clk,e11,op_count,e11_2);
	
	
	e12 <= '1' when (op_count = 12) else '0';
	div_e12 : entity work.enable_divider_2pulse(behavior)
		generic map(12)
		port map(clk,e12,op_count,e12_2);
	
	
	e13 <= '1' when (op_count = 13) else '0';
	div_e13 : entity work.enable_divider_2pulse(behavior)
		generic map(13)
		port map(clk,e13,op_count,e13_2);
	
	
	e14 <= '1' when (op_count = 14) else '0';
	div_e14 : entity work.enable_divider_2pulse(behavior)
		generic map(14)
		port map(clk,e14,op_count,e14_2);
	
	
	e15 <= '1' when (op_count = 15) else '0';
	div_e15 : entity work.enable_divider_2pulse(behavior)
		generic map(15)
		port map(clk,e15,op_count,e15_2);
	
	
	e16 <= '1' when (op_count = 16) else '0';
	div_e16 : entity work.enable_divider_2pulse(behavior)
		generic map(16)
		port map(clk,e16,op_count,e16_2);
	
	
	e17 <= '1' when (op_count = 17) else '0';
	div_e17 : entity work.enable_divider_2pulse(behavior)
		generic map(17)
		port map(clk,e17,op_count,e17_2);
	
	
	e18 <= '1' when (op_count = 18) else '0';
	div_e18 : entity work.enable_divider_2pulse(behavior)
		generic map(18)
		port map(clk,e18,op_count,e18_2);
	
	
	e19 <= '1' when (op_count = 19) else '0';
	div_e19 : entity work.enable_divider_2pulse(behavior)
		generic map(19)
		port map(clk,e19,op_count,e19_2);
	
	
	en_fn_reg <= e1_2 or e3_2 or e5_2 or e7_2 or e9_2 or e11_2;
	en_accum <= e13_2 or e14_2 or e15_2 or e16_2 or e17_2 or e18_2 or e19_2;
	
	
	-- fn register
	reg_fn : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en_fn_reg,reset,temp_out,fn);

		
	-- g0,...,g5 registers (past values)
	reg_g6_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e2_2,reset,temp_out,g6_past);
		
	reg_g5_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e4_2,reset,temp_out,g5_past);
		
	reg_g4_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e6_2,reset,temp_out,g4_past);
		
	reg_g3_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e8_2,reset,temp_out,g3_past);
		
	reg_g2_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e10_2,reset,temp_out,g2_past);
	
	reg_g1_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e12_2,reset,temp_out,g1_past);
		
	reg_f0 : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,e11_2,reset,temp_out,f0);
		
	reg_f0_past : entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk,en,reset,f0,f0_past);
		
	reset_accum <= (not(e1_2) and reset);
	--accumulator for final products and sums (v0*g0+...+v6*g6)	
	sum_accum_unclipped <= temp_out + sum_accum_buffered;
	sum_accum_clipped <= (35 => temp_out(35), others => not temp_out(35)) when temp_out(35) = sum_accum_buffered(35) and sum_accum_unclipped(35) /= temp_out(35) else sum_accum_unclipped;
	
	reg_sum_accum: entity work.reg_nbit(behavior)
		generic map(36)
		port map(clk, en_accum, reset_accum, sum_accum_clipped, sum_accum_buffered);
	
	
	-- clipping for fixed point Q13.23
--	sample_out <= sum_accum_buffered(23 downto 0) when 	sample_out_extended(35) = sample_out_extended(34) and
--																			sample_out_extended(35) = sample_out_extended(33) and
--																			sample_out_extended(35) = sample_out_extended(32) and
--																			sample_out_extended(35) = sample_out_extended(31) and
--																			sample_out_extended(35) = sample_out_extended(30) and
--																			sample_out_extended(35) = sample_out_extended(29) and
--																			sample_out_extended(35) = sample_out_extended(28) and
--																			sample_out_extended(35) = sample_out_extended(27) and
--																			sample_out_extended(35) = sample_out_extended(26) and
--																			sample_out_extended(35) = sample_out_extended(25) and
--																			sample_out_extended(35) = sample_out_extended(24) and
--																			sample_out_extended(35) = sample_out_extended(23) else
--																			(23 => sample_out_extended(35), others => not sample_out_extended(35));
	
	-- clipping for fixed point conversion Q18.18
	sample_out <= sum_accum_buffered(18 downto 0) & '0' & '0' & '0' & '0' & '0' when 	sample_out_extended(35) = sample_out_extended(34) and
																													sample_out_extended(35) = sample_out_extended(33) and
																													sample_out_extended(35) = sample_out_extended(32) and
																													sample_out_extended(35) = sample_out_extended(31) and
																													sample_out_extended(35) = sample_out_extended(30) and
																													sample_out_extended(35) = sample_out_extended(29) and
																													sample_out_extended(35) = sample_out_extended(28) and
																													sample_out_extended(35) = sample_out_extended(27) and
																													sample_out_extended(35) = sample_out_extended(26) and
																													sample_out_extended(35) = sample_out_extended(25) and
																													sample_out_extended(35) = sample_out_extended(24) and
																													sample_out_extended(35) = sample_out_extended(23) and
																													sample_out_extended(35) = sample_out_extended(22) and
																													sample_out_extended(35) = sample_out_extended(21) and
																													sample_out_extended(35) = sample_out_extended(20) and
																													sample_out_extended(35) = sample_out_extended(19) and
																													sample_out_extended(35) = sample_out_extended(18) else
																													(23 => sample_out_extended(35), others => not sample_out_extended(35));
--	
	
END Structural;