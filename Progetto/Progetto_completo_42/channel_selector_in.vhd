LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY channel_selector_in IS
	GENERIC (n: natural := 24);
	PORT ( 
		clk,en,res,fx_en : in std_logic;
		ch_sel : in std_logic_vector(4 downto 0);
		L_in_filtered, R_in_filtered : in std_logic_vector(n-1 downto 0);
		L_in, R_in : in std_logic_vector(23 downto 0);
		n_out : out std_logic_vector(n-1 downto 0)
		);
END ENTITY;

ARCHITECTURE Behavior OF channel_selector_in IS
	signal L_in_extended, R_in_extended : std_logic_vector(n-1 downto 0);
BEGIN
	
	PROCESS (clk, en, res, fx_en) IS
	BEGIN
		
		if (res = '0') then
			--reset
			n_out <= (others => '0');
		elsif rising_edge(clk) then
			if fx_en = '1' then
				if ch_sel = 20 then
					--R wet
					n_out <= R_in_filtered;
				elsif ch_sel = 21 then
					--L wet
					n_out <= L_in_filtered;
				end if;
			else
				if ch_sel = 20 then
					--R dry
					n_out <= R_in;
				elsif ch_sel = 21 then
					--L dry
					n_out <= L_in;
				end if;
			end if;
		end if;
		
	END PROCESS;
	
END Behavior;