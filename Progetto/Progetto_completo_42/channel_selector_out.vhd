LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY channel_selector_out IS
	GENERIC (n: natural := 24);
	PORT ( 
		clk,en,res : in std_logic;
		ch_sel : in std_logic_vector(4 downto 0);
		n_in : in std_logic_vector(n-1 downto 0);
		L_out, R_out : out std_logic_vector(n-1 downto 0)	
		);
END ENTITY;

ARCHITECTURE Behavior OF channel_selector_out IS
BEGIN
	
	PROCESS (clk, en, res, ch_sel)
	BEGIN
		
		if (res = '0') then
			--reset
			R_out <= (others => '0');
			L_out <= (others => '0');
		elsif rising_edge(clk) then
			if ch_sel = 20 then
				R_out <= n_in;
			elsif ch_sel = 21 then
				L_out <= n_in;
			end if;
		end if;
		
	END PROCESS;
	
END Behavior;