LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY complete_tb IS
END complete_tb;

ARCHITECTURE bench OF complete_tb IS
	SIGNAL Clock, Enable, Reset : STD_LOGIC := '0';
	SIGNAL DataIn: STD_LOGIC_VECTOR(23 DOWNTO 0) := "011111111111111111111111";  --Q1.23
	SIGNAL DataOut_Left,DataOut_Right,filt_data_left,filt_data_right,volume_factor,buf_temp,buf_cropped: STD_LOGIC_VECTOR(23 DOWNTO 0) := "000000000000000000000000";  --Q1.23
	SIGNAL writedata_left,writedata_right: STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAl buffer_amplified: STD_LOGIC_VECTOR(47 downto 0);
	SIGNAL op_count: STD_LOGIC_VECTOR(4 DOWNTO 0) := "00000";
	CONSTANT Tpw_clk : time := 10ns;

BEGIN

	--conter for operations
	counter: entity work.operation_counter
		GENERIC MAP(0,20,5)
		PORT MAP(Enable,clock,'1','1',op_count);
		
		
	
	-- filters (one filter per channel)
	filter_left: entity work.single_channel_filter(structural)
		port map(Clock,Enable,'1',op_count,DataIn,filt_data_left);
		
	filter_right: entity work.single_channel_filter(structural)
		port map(Clock,Enable,'1',op_count,DataIn,filt_data_right);
		
		
		
	vol_selector: entity work.volume_factor_selector(behavior)
		port map("0000",volume_factor);
	
	
	-- filter out -> channel selector -> multiplier
	ch_selection: entity work.channel_selector_in(behavior)
		port map(Clock,Enable,'1','1',op_count,filt_data_left,filt_data_right,DataIn, DataIn,buf_temp);
	
		
	buffer_amplified <= std_logic_vector(signed(volume_factor) * signed(buf_temp));	
		
	-- buffer assignations to writedata and cropping, pay attention to the clock, must be rollover of multiplier(?)
	buf_cropped <= buffer_amplified(43 downto 20);
	
	out_ch_selection: entity work.channel_selector_out(behavior)
		port map(Clock,'1','1',op_count,buf_cropped,writedata_left, writedata_right);


	clock_gen : process is
	begin
		Clock <= '1' after Tpw_clk, '0' after 2 * Tpw_clk;
		wait for 2 * Tpw_clk;
	end process clock_gen;														 
															 
	stimulus : process is
	begin
		Enable <= '0';  wait for 2 us;
		Enable <= '1';  wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';  wait for 2 us;
		Enable <= '1'; wait for 20 ns;
		Enable <= '0';
		wait;
	end process stimulus;
	
END ARCHITECTURE;
