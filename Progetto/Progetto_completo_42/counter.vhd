LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY counter IS
	GENERIC ( n_start : integer; n_stop : integer; k : natural);
	PORT ( 
		clk,en,res : in std_logic;
		n_out : out std_logic_vector(k-1 downto 0) := (others => '0')
		);
END ENTITY;

ARCHITECTURE Behavior OF counter IS
	signal count : std_logic_vector(k-1 downto 0) := (others => '0');
BEGIN

	PROCESS (clk, en, res) IS
	BEGIN
		
		if (res = '0') then
			count <= (others => '0');
		else 
			if (rising_edge(clk) and en = '1') then
				count <= count + 1;
				if (count = n_stop) then
					count <= std_logic_vector(to_signed(n_start,k)); 
				end if;
			end if;
		end if;
		
	END PROCESS;
	
	n_out <= count;
	
END Behavior;