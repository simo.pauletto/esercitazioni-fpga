LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;


ENTITY enable_divider_2pulse IS
GENERIC (n: natural := 1);
	PORT ( 
		Clk,en: IN STD_LOGIC;
		op_count : IN STD_LOGIC_VECTOR(4 downto 0);
		en_2 : OUT STD_LOGIC := '0'
		);
END enable_divider_2pulse;

ARCHITECTURE behavior OF enable_divider_2pulse IS
BEGIN

	process(clk) is
		variable count : natural := 0;
	begin
		if falling_edge(clk) then
			en_2 <= '0';
			if en = '1' and op_count = n then
				count := count + 1;
				if count = 2 then
					en_2 <= '1';
					count := 0;
				end if;
			end if;
		end if;	
	end process;
	
END behavior;