transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/clock_generator.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/audio_codec.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/audio_and_video_config.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_SYNC_FIFO.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_Slow_Clock_Generator.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_I2C_AV_Auto_Initialize.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_I2C.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_Clock_Edge.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_Audio_Out_Serializer.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_Audio_In_Deserializer.v}
vlog -vlog01compat -work work +incdir+C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42 {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/Altera_UP_Audio_Bit_Counter.v}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/enable_divider_2pulse.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/reg_nbit.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/volume_factor_selector.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/operation_counter.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/elliptic_filter_6order.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/counter.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/channel_selector_out.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/binToHex_signed.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/binary_to_hex_encoder.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/channel_selector_in.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/multiplexer_21_n.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/coeff_selection_mux.vhd}
vcom -93 -work work {C:/Users/simop/Documents/UniTS/esercitazioni-fpga/Progetto/Progetto_completo_42/single_channel_filter.vhd}

