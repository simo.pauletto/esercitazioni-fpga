LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY multiplier_sincronous IS
	GENERIC (n: natural := 24);
	PORT ( 
		clk,en,res : IN STD_LOGIC;
		n1,n2: IN STD_LOGIC_VECTOR(n-1 DOWNTO 0);
		n_out: OUT STD_LOGIC_VECTOR(n*2-1 DOWNTO 0):= (others => '0')
		);
END multiplier_sincronous;

ARCHITECTURE behavior OF multiplier_sincronous IS
BEGIN

	process(clk, res, en) is
	begin
		if (res = '0') then
			n_out <= (others => '0');
		elsif (rising_edge(clk) and en='1') then
			n_out <= n1*n2;
		end if;
	end process;
	
END behavior;