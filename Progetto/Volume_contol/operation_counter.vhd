LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

--- MISSING ROLLOVER

ENTITY operation_counter IS
	GENERIC ( n_start : integer; n_stop : integer);--; k : natural);
	PORT ( 
		start_op, clk,en,res : in std_logic;
		n_out : out std_logic;--std_logic_vector(k-1 downto 0) := (others => '0');
		execution : out std_logic
		);
END ENTITY;

ARCHITECTURE Behavior OF operation_counter IS
	--signal count : std_logic;--std_logic_vector(k-1 downto 0) := (others => '0');
	-- shared variable count : integer;
	signal count : integer;
	signal execute : std_logic;
BEGIN
	
	process (start_op, clk, en, res) is
	--variable execute : std_logic := '0';
	begin
		if (res = '0') then
			count <= 0;
		elsif (rising_edge(clk) and start_op = '1') then
			execute <= '1';
		elsif (rising_edge(clk) and en = '1' and execute = '1') then
			count <= count + 1;
			if (count = n_stop+1) then
				count <= n_start;
				execute <= '0';
			end if;
		end if;	
	end process;
	
	
	n_out <= '1' when count>0 else 
				'0' when count=0;
				
	execution <= execute;	
				
--	PROCESS (start_op, clk, en, res)is 
--	BEGIN
--	
--		if rising_edge(start_op) then
--			execute <= '1';
--		end if;
--		
--		if (res = '0') then
--			-- count <= '0';
--			count <= 0;
--		elsif (rising_edge(clk) and en = '1' and execute = '1') then
--			count <= count + 1;
--			if (count = n_stop+1) then
--				count <= n_start;
--				execute <= '0';
--			end if;
--		end if;
--		
--	END PROCESS;
--	
--	n_out <= '1' when count>0 else 
--				'0' when count=0;
	
--	process (count) is
--	begin
--		if (count>0) then
--			n_out <= '1';
--		elsif (count = 0) then
--			n_out <= '0';
--		end if;
--	end process;
	
	--n_out <= count;

	
END Behavior;