LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY volume_control IS
   PORT ( CLOCK_50, CLOCK2_50, AUD_DACLRCK   : IN    STD_LOGIC;
          AUD_ADCLRCK, AUD_BCLK, AUD_ADCDAT  : IN    STD_LOGIC;
          KEY                                : IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
          FPGA_I2C_SDAT                      : INOUT STD_LOGIC;
          FPGA_I2C_SCLK, AUD_DACDAT, AUD_XCK : OUT   STD_LOGIC;
			 SW											: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
			 LEDR											: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
			 
			 HEX0: OUT STD_LOGIC_VECTOR(0 TO 6);
			 HEX1: OUT STD_LOGIC_VECTOR(0 TO 6)
			 );
END volume_control;

ARCHITECTURE Behavior OF volume_control IS

   COMPONENT clock_generator
      PORT( CLOCK2_50 : IN STD_LOGIC;
            reset    : IN STD_LOGIC;
            AUD_XCK  : OUT STD_LOGIC);
   END COMPONENT;

   COMPONENT audio_and_video_config
      PORT( CLOCK_50, reset : IN    STD_LOGIC;
            I2C_SDAT        : INOUT STD_LOGIC;
            I2C_SCLK        : OUT   STD_LOGIC);
   END COMPONENT;   

   COMPONENT audio_codec
      PORT( CLOCK_50, reset, read_s, write_s               : IN  STD_LOGIC;
            writedata_left, writedata_right                : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK, AUD_DACLRCK : IN  STD_LOGIC;
            read_ready, write_ready                        : OUT STD_LOGIC;
            readdata_left, readdata_right                  : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
            AUD_DACDAT                                     : OUT STD_LOGIC);
   END COMPONENT;

   SIGNAL read_ready, write_ready, read_s, write_s : STD_LOGIC;
   SIGNAL readdata_left, readdata_right            : STD_LOGIC_VECTOR(23 DOWNTO 0);
   SIGNAL writedata_left, writedata_right          : STD_LOGIC_VECTOR(23 DOWNTO 0);   
   SIGNAL reset                                    : STD_LOGIC;
	
	SIGNAL filt_data_left, filt_data_right, buf_temp : STD_LOGIC_VECTOR(23 downto 0);
	SIGNAL en, ch_sel, en_mult	  							 : STD_LOGIC;
	
	signal click_count : std_logic_vector(3 downto 0);
	signal volume_factor, buf_cropped : std_logic_vector(23 downto 0);
	signal buffer_amplified : std_logic_vector(47 downto 0);
 
BEGIN
   reset <= not(KEY(0));

   --YOUR CODE GOES HERE
	
	en <= (read_ready AND write_ready);
	
	
	
-- filter_right: entity work.moving_average_8(behavior)
--		port map(en,'1','1',readdata_right,filt_data_right);
--		
--	filter_left: entity work.moving_average_8(behavior)
--		port map(en,'1','1',readdata_left,filt_data_left);
--
--		
--	writedata_left <= readdata_left when ((SW(0)='0') and (en = '1')) else
--							filt_data_left when ((SW(0) = '1') and (en = '1'));
--	
--	writedata_right <= readdata_right when ((SW(0) = '0') and (en = '1')) else
--							filt_data_right when ((SW(0) = '1') and (en = '1'));
--		



	-- counter with KEY(1) 	-> display logic
	--								-> volume factor selector
	click_counter: entity work.counter(behavior)
		generic map(-4,4,4)
		port map(not(KEY(1)),'1',not(reset),click_count);
		
	display_volume: entity work.binToHex_signed(structural)
		port map(click_count,HEX1,HEX0);
		
	vol_selector: entity work.volume_factor_selector(behavior)
		port map(click_count,volume_factor);
	
	-- counter for operations -> channel selector -> multiplier (to buffer)
	operations_counter: entity work.operation_counter(behavior)
		generic map(0,1)
		port map(en,CLOCK_50,'1',not(reset),ch_sel,en_mult);
		
	ch_selection: entity work.channel_selector(behavior)
		port map(CLOCK_50,'1',not(reset),SW(0),ch_sel,filt_data_left,filt_data_right,readdata_left, readdata_right,buf_temp);
		
	multiplier: entity work.multiplier_sincronous(behavior)
		port map(CLOCK_50,en_mult,not(reset),volume_factor,buf_temp,buffer_amplified);
	
	-- buffer assignations to writedata and cropping, pay attention to the clock, must be rollover of multiplier(?)
	buf_cropped <= buffer_amplified(43 downto 20);
	
	out_ch_selection: entity work.channel_selector_out(behavior)
		port map(CLOCK_50,'1',not(reset),ch_sel,buf_cropped,writedata_left, writedata_right);
	
	
   read_s <= en;
	write_s <= en;
			
   my_clock_gen: clock_generator PORT MAP (CLOCK2_50, reset, AUD_XCK);
   cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, FPGA_I2C_SDAT, FPGA_I2C_SCLK);
   codec: audio_codec PORT MAP (CLOCK_50, reset, read_s, write_s, writedata_left, 
	                             writedata_right, AUD_ADCDAT, AUD_BCLK, AUD_ADCLRCK,
										  AUD_DACLRCK, read_ready, write_ready, readdata_left, 
										  readdata_right, AUD_DACDAT);
										  
	
END Behavior;
